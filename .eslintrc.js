module.exports = {
    "env": {
        "browser": true,
        "node": true,
        "commonjs": true,
        "es6": true
    },
    "extends": "eslint:recommended",
    "rules": {
        "indent": [
            "error",
            4
        ],
        "quotes": [
            "error",
            "single"
        ],
        "semi": [
            "error",
            "always"
        ],
        "no-console": "off"
    },
    "globals": {
        "describe": true,
        "it": true,
        "before": true,
        "after": true
    },
    "parserOptions": {
        "sourceType": "module",
        "ecmaVersion": 2017
    }
};