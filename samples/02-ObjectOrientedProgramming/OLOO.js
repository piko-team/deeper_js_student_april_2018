var Parent = {
    whisper: function() {
        return this.name.toLowerCase();
    },
    init: function(name) {
        this.name = name;
    }
};

var p1 = Object.create(Parent);
p1.init('NAME1'); // with new construction and initialization happened at once

var p2 = Object.create(Parent);
p2.init('NAME2');

console.log(p1.whisper());
console.log(p2.whisper());


// what if we make more levels of linking?

var Child = Object.create(Parent);

Child.shout = function() {
    return this.name.toUpperCase();
};

var c1 = Object.create(Child);
c1.init('name1');
var c2 = Object.create(Child);
c2.init('name2');

console.log(c1.whisper());
console.log(c1.shout());
console.log(c2.whisper());
console.log(c2.shout());