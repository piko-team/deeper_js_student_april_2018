class Parent {
    constructor(name) {
        this.name = name;
    }

    whisper() {
        return this.name.toLowerCase();
    }
}

var p1 = new Parent('NAME1');
var p2 = new Parent('NAME2');

console.log(p1.whisper());
console.log(p2.whisper());

class Child extends Parent {
    constructor(name) {
        super(name);
    }

    shout() {
        return this.name.toUpperCase();
    }
}

var c1 = new Child('name1');
var c2 = new Child('name2');

console.log(c1.whisper());
console.log(c1.shout());
console.log(c2.whisper());
console.log(c2.shout());
console.log(c2.constructor);

// takeaway
// classes are mainly syntactic sugar for prototypal inheritances