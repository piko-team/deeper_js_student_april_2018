// Draw conceptual model with .prototype, .constructor, [[Prototype]], functions are circles, objects are squares
// Remember about Object function and its prototype


function Parent(name) {
    this.name = name; // state in individual objects created from Parent constructor function
}

// shared behavior on prototypes
Parent.prototype.whisper = function() {
    return this.name.toLowerCase();
};

var p1 = new Parent('NAME1');
var p2 = new Parent('NAME2');

console.log(p1.whisper());
console.log(p2.whisper());

// isn't constructor function a distraction?


// what if we make more levels of linking?


function Child(name) {
    Parent.call(this, name);
}

Child.prototype = Object.create(Parent.prototype); // we throw away old Child.prototype that will be GC collected
// we could do Object.setPrototypeOf(Child.prototype, Parent.prototype) in ES6

Child.prototype.shout = function() {
    return this.name.toUpperCase();
};

var c1 = new Child('name1');
var c2 = new Child('name2');

console.log(c1.whisper());
console.log(c1.shout());
console.log(c2.whisper());
console.log(c2.shout());
console.log(c2.constructor); // why? hint: line 28