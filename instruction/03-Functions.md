# Functions (2h)

Let's take a step back. Maybe we've gone too far with the constructors, classes etc.
Let's take a closer look at the most important (in my opinion) part of the
language: functions. Maybe they can do much more than we thought and are a good enough
abstraction for most of our problems.
Functions can be used as modules, constructors, event handlers, strategies (GoF pattern) etc.
So let's get to know them at a deeper level than most people ever do.

## Execution context modeling (5 min)

Quiz - what is it gonna print?
```
var name = "global";

function print() {
    console.log(name);
    var name = "local"; // shadowing
    console.log(name);
}

print();
```

Let's unpack JS internals to understand why.

Every time we call a function JS creates so-called execution context.
All our JS programs can be visualized as a stack of execution contexts where
only one context can be active at a given time.

```
|Active EC|
|...      |
|EC       |
|EC       |
|Global EC|
-----------
```

We can model execution context as an object:
```
var executionContext = {
    lexicalEnvironment/variableObject: [arguments, fn declarations, variable declarations AND later assignments],
    scopeChain: [current lexicalEnvironment + all parent lexicalEnvironments],
    thisValue: invocationContextObject
};
```

Every call to the executionContext has two stages:
* creation stage
  * create scopeChain
  * create arguments, function and variable declarations
  * determine thisValue
* execution stage
  * assign values to variable declarations and execute the code

Go to 03-Functions/exercise/executionContextLexicalEnvironment.js

Our goal is to fill in lexicalEnvironment object state after creation and execution stage

## Hoisting: be the compiler (10 min)

Hoisting is just a metaphor for what JS compiler really does.
Speaking of compilers, yes, JS is compiled. And then executed straight away.
Just because there's no binary file like in Java doesn't mean it's not compiled.
Going back to hoisting it's a subtle way how declarations (not assignments) are
being attached to the function scope (but not a block scope).
To be precise, function declarations and variable declarations (not assignments)
are moved to the beginning of the current function scope.

Let's do a quick quiz where we pretend to be a compiler.
Go to exercise/hoistingQuiz1.js and without running code try to
guess what will be printed.

Now let's make it more tricky.
Go to exercise/hoistingQuiz2.js and rewrite the code to what really happens under the hood.

And now let's add function declarations to the mix.
Go to exercise/hoistingQuiz3.js and rewrite the code as we did before.

## Functions as a scope mechanism (10 min)

What is scope? The answer to where to look for things.
Function is a default scope for the variables declared with the var keyword.

Go to exercise/executionContextScopeChain.js and model lexical environment and scope chain
for all execution contexts.

Once you're done with the exercise while we're still on the subject of scope lookup,
think about two common errors in JS: ReferenceError and TypeError.
How can you cause each of those?

## Block scope in ES6 (15 min)

ES6 introduces block scoped variables with let and const.
In this exercise we gonna role play a job interviewer where we ask questions about
 block scoping in JS.

As a refresher/preparation please go to theory/blockScopeCharacterizationTest.js and
go through the tests that capture the essence of block scoping in JS. I tried to cover
 almost everything there is to know about block scoping in those tests.
Prepare 3 interview questions that will verify if a candidate understands block scoping in JS.

## Dissecting a first line of a function (10 min)

function foo(x) { // <------ we gonna focus on this line
 // we're not interested in the body in this exercise
}

Q: What is a difference between arguments and parameters?

Q: How to check the number of function parameters and number of function arguments?

Q: How to check a function name?


Q: Why function names are important?


Q: Given this function:
```
function foo(x = Math.random()) {
    console.log(x)
}
```
Is Math.random() eager or lazy?
If we invoke this function multiple times are we gonna get the same value or different values each time?

```javascript
function mandatory() {
    throw new Error('Missing parameter');
}
function foo(mustBeProvided = mandatory()) {
    return mustBeProvided;
}
```

Q: What is the ... operator called in (1) and (2)?
```
function foo(...args) { 
}
var arr = [1,2,3];
foo(...arr); 
```

## Functions and this binding (15 min)

Lexical scope means where to look for things. In a function or in a block?
Lexical means that scope is defined in a lexing phase
(tokenization/lexing -> AST -> machine code) not at runtime.

On the other hand, "this" binding is dynamic.
Where to look for this reference? We have to check how the function is called.

There are 4 potential this bindings depending on how we invoke a function.
The bindings have a fixed precedence.
Go to theory/thisBindingCharacterizationTest.js to see all 4 bindings in action.

Sometimes we want to pass a reference to a function without dynamic this.
That's what the Function.bind() is used for.
Go to exercise/bindTest.js and implement your own bind function that behaves
as Function.bind()
Remove skip on describe.

Summary:
In your code if you use bind too often the question is if you need dynamic this at all.
Maybe you're better off with lexical scoping.

Thisless programming is a trend to avoid using this in JS code to make it easier
to reason about it by just looking at lexical bindings.

## Thisful vs thisless programming (20 min)

In this exercise we gonna see different ways of writing the same code:
* thisful programming with .prototype
* thisful programming with classes
* thisful programming with Object.create()
* thisless programming

Go to thisfulVsThislessTest.js and make the tests green.

## Performance benchmarking (5 min)

First observation: most of the time we shouldn't write our own performance benchmarking tool
unless we're very good at statistics.
On the other hand it's risky to make your benchmark dependant on some website (jsperf anyone?)

In this exercise we gonna run performance benchmark comparing 4 programming styles in JS.
* prototype based
* class based (syntactic sugar for above)
* closure based (my favourite)
* object linking based (OLOO)

Go to theory/performanceBenchmark.js and run it on your machine. It may take some time.
Is the result significant for your applications?
Do you create thousands of objects on a critical path or mostly when the app starts?
Remember that performance discussions make sense in the context of a given application.

## Memory usage (5 min)

We gonna compare prototype based solutions with closure based solution in terms of memory usage.
Go to theory/memoryUsage.js and run the code

How many objects do you create in your own apps?

## Function improvements in ES6 (10 min)

I prepared a summary of ES6 function enhancements in theory/es6FunctionsTest.js
Let's go through the examples.

## Arrow functions (15 min)

Ok, time for the last exercise in this section.
I noticed that many people started automatically
rewriting all their functions to the arrow format without understanding nuances.
Remember that arrow function are not a replacement for all regular functions.

What are the pros and cons of arrow functions?
When should you use arrow functions?
When should you NOT use arrow functions?
Write down examples of all possible arrow function variants depending on the number
of arguments and type of their body.


