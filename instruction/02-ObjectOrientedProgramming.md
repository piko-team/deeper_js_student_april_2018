# Object-Oriented Programming (3h)

In this section we gonna learn there's several different ways to do OO programming
in JS.
* OO with constructor function, new and prototype
* Objects linking with Object.create()
* OO using syntactic sugar of ES6 classes
* OO without this

Many people think OO is impossible w/o classes.
For OO you need objects (state + behavior travelling together in time) not classes.
Paradoxically, it's easier to learn OO in JS w/o knowing Java or C#.

## OO with constructor function, new and prototype (15 min)
In JS constructor is a function invoked with a new operator.
In Java constructor is a function attached to a class and used to create
objects of that class.

Open file samples/02-ObjectOrientedProgramming/classicalOO.js
For now we're only interested in the constructor function Parent. Our task is to draw
a conceptual diagram of the program (ignore Child function for now).
Pass a marker, everyone adds one thing.

Required elements:
* functions - use circle to represent those
* objects - use squares to represent those
* .prototype - a link to constructor function's prototype
* [[Prototype]]/```__proto__``` - a link to object's' prototype
* .constructor - a link to constructor function

How does the [Object](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object) and Object.prototype fit into this model?
How does the [Function](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function) and Function.prototype fit into this model?

## OO with new and prototype - "inheritance" (10 min)

Modify the model from the previous exercise and add Child constructor function.
How is .constructor looked up in the chain?

When we throw away original .prototype we loose .constructor link. But we can walk up
the prototype chain and get .constructor from Parent.prototype

## Objects linking (10 min)

OLOO.js has a different way of linking without constructor function, new and .prototype.
Our task is to draw a conceptual model of the program. Just as we did in the previous
exercises we start with a simple model and a separate model with Parent alone and then we
enhance it with Child.

What are main differences between classical OO and object linking?

Check OLOO_ES6.js to see how we could use setPrototypeOf instead of Object.create()
 and also how we use enhanced object literals to remove boilerplate.

## Classical OO with ES6 classes (10 min)

Rewrite classicalOO.js to use classes. Now let's draw a conceptual diagram.
What are your observations?

Now, let's add a static methods to Parent and Child. Where would you place the static
methods on our diagram?

## Classes in depth (10 min)

Open test/02-ObjectOrientedProgramming/theory/classesCharacterizationTest.js

Learning tip: I created those examples when I was learning about ES6 classes.
It's a cool learning method to write down what you learn as unit tests.
I find it much easier to go back to unit tests than to look for something in a book.
As an added bonus you can verify what features are supported by your JS runtime.

Let's analyze the most interesting examples from the test (gotchas and inheritance from built-ins).


D.Crockford: people will go to their graves not knowing how miserable 
their life was
by not learning the true functional nature of JS and 
pretending it has classes

## DOT and DOTCALL ([[Get]] and [[Call]]) (5 min)

Go to exercise/dotcallTest.js. Implement custom DOTCALL function using provided
DOT function.

Hint:
* fn.call(context, 1,2,3)
* fn.apply(context, [1, 2, 3]) - calling a function with explicit this context and arguments

## NEW (10 min)

In this exercise we gonna implement our own equivalent of the new operator.

We gonna work in exercise/newTest.js

What is new doing under the hood?
* it creates a new object
* for the newly created object it sets [[Prototype]]/```__proto__```` to the constructor function's' .prototype
* it calls constructor function with the new object set as a context
* if the constructor function call returns an object we return it
* otherwise we return the newly create object from step 1

More: [this](http://stackoverflow.com/questions/1646698/what-is-the-new-keyword-in-javascript) stackoverflow question

## INSTANCEOF (10 min)

The same way we implemented our own NEW we gonna implement our own INSTANCEOF

Go to exercise/instanceofTest.js and make the tests green

Hints:
* we have to walk up the prototype chain looking for the constructor function .prototype
* walking up the chain can be done with ```__proto__``` or Object.getPrototypeOf()

## Object.assign() (15 min)

For years, many JS developers have been writing or using library function similar to
Object.assign() that was introduced in ES6.
What is it used for?

1) One step defaults

var config = {
url: ''
}

var devconfig = {
url: ''
}

```
var options = Object.assign({}, currentState, newState);
```

2) Adding methods to the object in one swoop

```
Object.assign(SomeClass.prototype, {
    someMethod(arg1, arg2) {
        ···
    },
    anotherMethod() {
        ···
    }
});
```

3) Cloning

```
function clone(orig) {
       return Object.assign({}, orig);
}
```

4) Copying behavior

```
Object.assign(child, parent); // now child has behavior from the parent
```

To better understand what Object.assign() does we gonna implement our own function
extends.
Go to exercise/extendTest.js and make the tests green.

IMO, this is the easiest OO variant in JS.

Please note that Object.assign() makes a shallow copy?
What about deep copy? Deep copy is more complicated due to things like e.g. cyclic dependencies.
So when making a deep copy you need to have some opinion about how to copy.
One deep copy with nice defaults is [lodash.merge](https://lodash.com/docs/4.17.4#merge).
There's also [lodash.mergeWith](https://lodash.com/docs/4.17.4#mergeWith) that allows for a customizer.


## Destructuring data structures (10 min)

Destructuring is advanced declarative assignment with patterns.
It's like saying: this is the shape of stuff I expect.

Go to exercise/destructuringTest.js and write destructure author.
If you need a refresher on destructuring: theory/destructuringCharacterizationTest.js

# Homework

## instanceof and natives (10 min)

In part 1 typeof was telling us the difference between primitive types and objects.
But sometimes we want to distinguish between objects.

Go to theory/instanceofForNativesTest.js to learn about the internal [[Class]] property

Exercise: write more generic typeof in exercise/typeofTest.js

The technique describe in here can be found [in the wild](https://github.com/angus-c/just/tree/master/packages/object-typeof)


## Traversing object properties (15 min)

ES5 opened meta programming capabilities:
- define property internals
- navigate object properties

Meta Object API
property: value, writable, configurable, enumerable, get, set

```
var o = {foo: "bar"};
var o = Object.create(Object.prototype);
Object.defineProperty(o, 'foo', {
    value: 'bar',
    writable: true,
    enumerable: true,
    configurable: true
});
```

There's many different ways to traverse object properties in JS.
* Object.keys(obj) : Array<string> - all string keys of all enumerable own properties
* Object.getOwnPropertyNames(obj) : Array<string> - all string keys of all properties (even non-enumerable)
* Object.getOwnPropertySymbols(obj) : Array<symbol> - all symbol keys of all properties
* Reflect.ownKeys(obj) : Array<string|symbol> - all keys of own properties
* for (let key in obj) - all string keys of all enumerable properties in the prototype chain

If it's confusing don't worry. I need to look them up on a regular basis.
Please note that Object.keys() and Object.getOwnPropertyNames() don't walk up the prototype chain.
Our job will be to implement Object.allKeys() and Object.getAllPropertyNames() that can walk up the prototype chain.

Head over to exercise/objectKeysAndPropertiesTest.js and make the tests go green.

Hint: Array.concat() joins arrays