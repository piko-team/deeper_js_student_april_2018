# Types (3h)

## Intro

Plan: (value) types, === and special values, type coercion, ==


## Warm-up koan - typeof (10 min)

JS has types. Value types. Not reference types.

* Go to test/01-Types/exercise/typeofTest.js 
* Remove it.skip to enable test 
* make the test go green 

Note: 
* as you solve the problem you can also remove es-lint annotations 


## ToBoolean (15 min)

Many JS developers never look into the language spec. While it's not something you have to read
every day, it's important to get familiar with it. 
In this exercise we gonna implement internal language function ToBoolean. Spec calls those functions
abstract operations. Our ToBoolean function will perform explicit conversion to a value of type boolean.
 
* Go to [spec](http://www.ecma-international.org/ecma-262/7.0/index.html#sec-toboolean)
* Test drive your ToBoolean implementation based on the spec (ToBooleanTest.js)

Constraints:
* we can't use ==, Boolean() and !! as they perform coercion themselves and we're trying to implement it ourselves
* we should use typeof and ===

## Type conversion graph (15 min)

In this exercise your goal is to draw a type conversion graph between string, number and boolean.
List as many conversion options as you know.
In the graph nodes are types connected with conversion links
Trainer draws edges, group fills in the links

Hints:
* If you run out of ideas please check theory/typeConversionTest.js for examples

## == (45 min)

We gonna write eq(x, y) function that works the same way as ==. == is sometimes called
loose equality operator, but in spec it's called abstract equality operator

* go to [spec](http://www.ecma-international.org/ecma-262/7.0/index.html#sec-abstract-equality-comparison)
* Using TDD implement your own version of ==
* Use the ubiquitous language from the spec when writing your tests
* remember not to use == in your own implementation, only ===, typeof and explicit
 type conversion with Number() is allowed
* https://gist.github.com/kwasniew/30c82cb6cc4ba2a7121d215980508e93
 

# Homework

## Numbers close (5 min)

Each type has it's characteristics. Let's look at some nuances of the number type.

* File: numbersCloseTest.js 
* make the last test go green (remember to remove skip)

Note: 
* as you solve the problem you can also remove es-lint annotations 


## Fixing === with Object.is() (15 min)
 
ES6 introduces Object.is() method that works similarly to === but it also
handles special values NaN and -0 in a more intuitive way.

* Go to file objectIsTest.js 
* implement your own is() function that works the same way as Object.is() 
* remember to remove all it.skip() 

Hints:
* NaN is the only value not equal to itself
* 1/+0 = Infinity
* 1/-0 = -Infinity
* -0 may be the result of multiplication e.g. -1 * 0

## ToPrimitive (30 min)

The purpose of this exercise is to learn how objects are converted to numbers and strings

```
1 + {}
"1[object Object]"
```

* Implement algorithm from the [book](http://speakingjs.com/es5/ch08.html#toprimitive)
* tests for the algorithm are in ToPrimitiveTest.js