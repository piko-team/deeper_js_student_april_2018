# Functional Programming I (3.5h)

FP is about using functions in a very specific way.
FP is also a spectrum.
On one side it gives us tools that increase level
of abstraction, improve reusability and bring simplicity to your code, on the other side 
we can stumble upon academic absurdities, that don't help to solve problems of mere mortals.
JS helps to gently enter the world of FP and escape from if statement, loops and
gradually purify our code.
Ifs and loops are gotos of our times.

## No perfect definition (15 min)

What do people have in mind when saying functional programming?
In groups find the 10 most important characteristics of FP that you've heard of.

## Array.map() (10 min)

JS provides higher order functions out of the box.
The most prominent trio: map/filter/reduce

https://twitter.com/steveluscher/status/741089564329054208

Go to 04-FunctionalProgrammingI/exercise/arrayMapTest.js and implement
just enough code to make the tests green.
Please don't use Array.prototype.map as it's our goal to implement it ourselves.
The array you iterate over is available under this.

## Array.filter() (5 min)

Filter means filter in the items I want. May be counter intuitive to some people
 who we'd assume filter out.
Go to exercise/arrayFilterTest.js. Make it green.

## Array.reduce()  (20 min)

[1,3,5].reduce((acc, curr) => acc + curr, 0)
(0, 1) => 1
(1, 3) => 4
(4, 5) => 9
[1,3,5].reduce((acc, curr) => acc + curr)
(1, 3) => 4
(4, 5) => 9

Reduce is the loop equivalent in the FP world. You can implement map/filter and other
higher level functions using reduce. So reduce is a low level operation.
Reduce is also a key concept to understand libraries like Redux.
Go to exercise/arrayReduceTest.js. This time we gonna implement it with
corner cases. The tests will guide you.

## Paradigms

Interlude - functional paradigm. Paradigm - the way we think about the world.
Paradigms enable progress. We can think about more things at a higher level. 
All models are wrong. Some are just more useful.
There's a  tendency to remove things: structured programming => 
removed goto -> OO programming -> remove this -> FP programming
Programming across paradigms: 
https://www.youtube.com/watch?v=Pg3UeB-5FdA

## Utility belts
List operations with map/filter/reduce or newly added find is just a beginning.
To avoid reinventing the wheel
new libraries are created such as: underscore, lodash, ramda.
They are utility belts that make up for the missing standard library functions.
I think it's' important to know what they do under the hood.
One library that's very good for learning purposes is [just](https://github.com/angus-c/just).
In general utility belts do things in 3 categories:
* object utilities (e.g. extend, values, pick, omit)
* array utilities (e.g. takeWhile, flatten, zip, union, intersect, partition)
* function utilities (e.g. compose, curry, partial)

## Array utility takeWhile() (10 min)

Implement takeWhile in exercise/takeWhileTest.js.
As an extra exercise implement it using Array.every() function.

Please note that we don't modify Array prototype anymore.

## Array utility flatten() and flatMap() (15 min)

Implement flatten and flatMap in exercise/flattenTest.js

Hints:
* Array.isArray() performs array check
* flatMap first performs map on each item and then flattens the result
* flatten in this exercise is a deep flatten that goes to any depth

## Advanced flatten, map and filter in practice (15 min)

Go to exercise/flattenInActionTest.js and make the tests green.
Note: you can't use indexed array access.
Only flatten, map and filter are allowed.

## Array utility zip() (15 min)

Implement zip in exercise/zipTest.js

## Object utility pick() (10 min)

Implement pick() in exercise/pickTest.js

## Immutability (15 min)

Why is it so easy to work with numbers and strings? Because they are immutable.
I know that no operation will modify them and I can predict what program will do
without digging deep into the implementation.
On the other hand, arr.push(5) modifies the original array (and returns modified array length
instead of a new array). It's much more difficult to reason about.
In the immutable world when we need a new value we create it and we don't touch the old one.

In real world programs state changes so what can we do?
* Object.freeze() state and copy before modification
  * var newArr = [...oldArr, 4];
  * var newUserRecord = Object.assign({}, oldUserRecord);
* use persistent data structures library e.g. immutable.js or mori
  * similar to git history
  * more efficient than full copy on each modification
  * good for large objects with lots of small modifications
* we can minimize state changes to the minimum (e.g. one state object and lots of pure functions)

Go to exercise/modificationPreventionTest.js
Implement you own customFreeze method that works like Object.freeze(). Remaining unit tests are there to give you hints
about modification prevention options in JS.

Hints:
* Use preventExtensions() to prevent new properties from being added
* Don't use Object.seal() as it doesn't make existing properties non-writable
* Use Object.defineProperty() to make existing properties non-writable and non-configurable
* Object.keys(o) gives your object keys
* Object.getOwnPropertyDescriptor(o, property) gives you existing property descriptor


## Summary

The way the language is designed influences how we can solve problems. Yes we can try to make Java
 or some other lang we're familiar with in JS and many people do. 
 But then you're fighting with the language not playing along with it. 
 Play to your language strengths.
 
# Homework

## Array.find() (15 min)

Array.indexOf() uses === under the hood. What if we want to have our own algorithm
for finding the first element. ES6 introduces Array.find() and Array.findIndex()

In the spirit of this course's "involve me and I learn" let's implement custom find().
Go to exercise/arrayFindTest.js and build your own Array.customFind.

## Deep freeze (10 min)

Object.freeze() is a shallow freeze. So let's implement deep freeze.
Hint: you can use Object.freeze(o) and Object.isFrozen(o)
Write your implementation in exercise/deepFreezeTest.js

## Referential transparency and memoization (15 min)

In this part we gonna learn how to avoid duplicated function calls especially in the
context of recursive calls.
Go to exercise/memoizationTest.js and implement memoize function that wraps original
function and allows to cache previously computed results for the same input arguments.

## Forcing one function argument (5 min)

First type of adjustment we gonna look at is forcing single argument in a function.

Our exercise: 08-FunctionalProgrammingII/exercise/unaryTest.js

## Reversing arguments (5 min)

Another adjustment useful for joining functions from different libraries is reverse arguments
operation.

Our exercise: 08-FunctionalProgrammingII/exercise/reverseArgumentsTest.js

## Partial application (10 min)

Let's analyze the following example:
```
function buildUrl(url, path, id) {
    return `${url}/${path}/${id}`;
}
function getOrderById(id) {
    return buildUrl('http://example.com', 'order', id);
}
function getFixedOrderByHost(host) {
    return buildUrl(host, 'order', '1');
}
```
We create two functions that set two leftmost or two rightmost arguments of buildUrl to fixed values.
Effectively we reduce arity of the buildUrl function from 3 to 1.
Can we automate it?

Go to exercise/partialApplicationTest.js and implement partial application helper.

## Currying (15 min)

What do JS functions do when we don't pass all arguments?
In the functional world we do currying.
```
f(a,b,c)
f(a) -> f(b, c)
f(a, b) -> f(c)
f(a, b, c) -> result
```
So when you call a function with fewer arguments than it expects, you get
a function that takes the remaining arguments.

Go to exercise/curryingTest.js and implement curry function

## Currying in the real world (5 min)

In OO world it's very common to pass things that don't change during app
lifetime as constructor arguments and use function parameters
for things that constantly change at runtime.
e.g.
```
class UserDataFetcher {
    constructor(ajax) {
        this.ajaxClient = ajax;
    }

    fetchById(id) {
        return id != null ? this.ajaxClient(id) : null;
    }

    ...
}
```
Let's see the functional inspired equivalent (to be precise injecting functions with side-effects
is not purely functional).
Go to exercise/curriedDITest.js and see how our curry function can be used.

## Point free style (10 min)

Point is a fancy word for parameter. So point free style is programming without
explicit parameters. Let's look at some examples:

```
// let's get rid of x
function foo(x) {
    return bar(x);
}
// can be replaced with
var foo = bar;
```
```
function processPromise(data) {
    // do something
}
// let's get rid of result
...then(function(result) {
    return processPromise(result);
});
// can be replaced with
...then(processPromise);
```

Now it's your turn to refactor code here: /exercise/pointFreeTest.js
