# Async (2.5h)

First let's get some wording right.
Concurrent vs parallel is best explained by [this picture](https://joearms.github.io/images/con_and_par.jpg)
So JS is concurrent, but not parallel (unless we use e.g. WebWorkers).

## Single threaded event loop (5 min)

We have one thread available for our business logic. And an event loop controls
access to this one thread.
Everything outside our program is run in parallel (setTimeout, XmlHttpRequest, fs, http)

I recommend playing around with the event loop/callstack/callback queue visualization
at [this address](latentflip.com/loupe/)

Pros:
* no deadlocks
* run to completion guarantee
* one stack to reason about
* memory efficient

Cons:
* program must not block (never block, never wait, finish fast),
* programs written inside out - callbacks

Good for I/O heavy apps (UI, some backends - orchestrators, BFF, CRUD). Not for CPU heavy.


## Callbacks (10 min)

Go to 05-Async/exercise/callbacksFlowTest.js

What are the most important problems with this code?

Our task is to fix ordering issue in parallel invocation.

## Promises intro

What are promises? Two words. Future Value.
Declare how things will works when the value eventually resolves.
Box where the value will hatch eventually.

Many people saw promises in jQuery for the first time and thought it's just a different API.
```
// before
$.get(some_url, callback);

// after
var promise = $.get(some_url); // return promise immediately
promise.then(callback);

// one liner
$.get(some_url).then(callback);
```

But promises are not only about a new API. They are about gaining trust in our code.
Trust that things will be invoked at most once and invoked asynchronously.

Let's analyze promiseCharacterizationTest.js to learn the ins and outs of Promises.

What I learned from implementing my own promise implementation
https://github.com/kwasniew/apluspromises
* testing for spec compliance
* state machine - pending to fulfilled/rejected. resolve can either fulfil or reject
* reactions - many listeners
* nested promises in then - hard part
* Promise.resolve/reject - statics
* thenables - why different libs play together nicely
* async

## Zalgo (10 min)

Code can be sometimes sync, but sometimes async.
Live coding.

## Promise flow (10 min)

Let's rewrite previous exercise to Promises


## Async flow (5 min)

Let's rewrite our initial exercise to async/await

# Homework

## Promises - promisify callback (5 min)

Promises don't remove callbacks, they just fix the trust issue.
You can still end up with nested promises.
As it's not an intro course we gonna focus on more advanced use cases.
First exercise will be to implement generic promisify function that will adapt
node style callbacks to promises.

Make the test green at: 05-Async/exercise/promisifyTest.js

In the wild: [Q library](https://github.com/kriskowal/q#adapting-node)

## Promise.finally() (15 min)

Let's implement something resembling finally clause in error handling, but
for promises.

Go to exercise/promiseFinallyTest.js and make it green

Hint: Promise.resolve(x) turns x into a promise

## Promises - timeout utility (10 min)

exercise/promiseTimeoutTest.js - you know the rules

This time we implement it as util function rather than something on the Promise
prototype.
I've seen some interesting debates about whether things should live on the
prototype or as separate utils in the user space.

## Promise.first() (20 min)

Promises come out of the box with Promise.all() and Promise.race().
There are other abstractions we can build to orchestrate promises.

In this exercise we gonna implement Promise.first as described by tests
in exercise/promiseFirstTest.js



