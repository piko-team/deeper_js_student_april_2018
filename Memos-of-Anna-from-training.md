# LINKI
[https://docs.google.com/document/d/1Tv4YnmeAewmydewlWsLZjZfsJewxTTTG-NIFSjleaoc](https://docs.google.com/document/d/1Tv4YnmeAewmydewlWsLZjZfsJewxTTTG-NIFSjleaoc)
[https://ebookpoint.pl/ksiazki/tajniki-jezyka-javascript-na-drodze-do-bieglosci-kyle-simpson,tjndro.htm](https://ebookpoint.pl/ksiazki/tajniki-jezyka-javascript-na-drodze-do-bieglosci-kyle-simpson,tjndro.htm)
[https://twitter.com/steveluscher/status/741089564329054208](https://twitter.com/steveluscher/status/741089564329054208)
[http://latentflip.com/loupe/?code=JC5vbignYnV0dG9uJywgJ2NsaWNrJywgZnVuY3Rpb24gb25DbGljaygpIHsKICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gdGltZXIoKSB7CiAgICAgICAgY29uc29sZS5sb2coJ1lvdSBjbGlja2VkIHRoZSBidXR0b24hJyk7ICAgIAogICAgfSwgMjAwMCk7Cn0pOwoKY29uc29sZS5sb2coIkhpISIpOwoKc2V0VGltZW91dChmdW5jdGlvbiB0aW1lb3V0KCkgewogICAgY29uc29sZS5sb2coIkNsaWNrIHRoZSBidXR0b24hIik7Cn0sIDUwMDApOwoKY29uc29sZS5sb2coIldlbGNvbWUgdG8gbG91cGUuIik7!!!PGJ1dHRvbj5DbGljayBtZSE8L2J1dHRvbj4%3D](http://latentflip.com/loupe/?code=JC5vbignYnV0dG9uJywgJ2NsaWNrJywgZnVuY3Rpb24gb25DbGljaygpIHsKICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gdGltZXIoKSB7CiAgICAgICAgY29uc29sZS5sb2coJ1lvdSBjbGlja2VkIHRoZSBidXR0b24hJyk7ICAgIAogICAgfSwgMjAwMCk7Cn0pOwoKY29uc29sZS5sb2coIkhpISIpOwoKc2V0VGltZW91dChmdW5jdGlvbiB0aW1lb3V0KCkgewogICAgY29uc29sZS5sb2coIkNsaWNrIHRoZSBidXR0b24hIik7Cn0sIDUwMDApOwoKY29uc29sZS5sb2coIldlbGNvbWUgdG8gbG91cGUuIik7!!!PGJ1dHRvbj5DbGljayBtZSE8L2J1dHRvbj4%3D)
[https://gist.github.com/kwasniew/585466f9b41643dd8cf032610edb297d](https://gist.github.com/kwasniew/585466f9b41643dd8cf032610edb297d)

# TYPY
* Jest string, nie ma char.
* Kazda zmienna liczbowa to number.
* Dlaczego null jest obiektem? Błąd językowy.
* Zmienna zadeklarowana bez wartosci jest undefined.
* Typeof ze zmiennej niezadeklarowanej tez jest undefined !
* Wszystko co nie jest typem prostym, jest obiektem !!!

- `Doczytaj`: Specyfikacja ToBoolean
- `Doczytaj`: Truthy / falsy

* isNaN() zepsuta funkcja, zamiast tego korzystaj z Number.isNaN(“aaaaa”)
* NaN === NaN daje false ☹
* x !== x to inne sprawdzenie, czy NaN

## Konwersja typów

* String to boolean: Boolean(argument)
* Number to boolean: Boolean(argument)
* Inne sposoby rzutowania: [zobacz rysunek]

- `Doczytaj`: Konwersja typów vs parsowanie
- `Doczytaj`: WTFJS

* 123.toString() nie zadziała, ale (123).toString() zadziała, bo JS myśli, że to number zmiennoprzecinkowy
* Z koercją typów ==
* Bez koercji typów ===
* Performance: === jest szybsze
* == ma sens w 1 przypadku: if(x == null) zamiast if(x === null || x ===undefined)
* MIT: nigdy nie używaj implicit koercji  - Wyjątek to if i !!
* Żeby robić programowanie obiektowe, nie musisz mieć klasy!
* Def OOP: Stan i zachowanie podróżują razem w czasie.

# FUNKCJE

* Za każdym razem przy deklaracji funkcji, oprócz funkcji tworzy się też powiązany z nią obiekt prototype.
* Funkcje są obiektami!
* New to specjalne wywołanie funkcji, robi 4 rzeczy:  
    + wywołuje funkcję, 
    + magicznie tworzy nowy obiekt, 
    + następnie wywołuje funkcję z nowo powstałym obiektem,  
    + ustawia relację nowego obiektu do prototype \_\_proto\_\_ lub [[prototype]]
* .prototype zawsze występuje na funkcji, a \_\_proto\_\_ na obiekcie

- `Doczytaj`: Fn.call(context, 1,2,3)
- `Doczytaj`: Fn.apply(context,[1,2,3])

* instanceOf – w JS jest czym innym niż w normalnych językach programowania
* c1 instanceOf Parent – czy gdziekolwiek w hierarchii prototypów jest obiekt [kwadrat], na który wskazuje funkcja konstruktora  [kółko] Parent
* getPrototypeOf() to samo co \_\_protype\_\_
 *nie używać: new, class, .prototype, instanceOf, object.create, this
* używaj: assign({}, currentState, newState), mutujemy pierwszy, doklejając kolejne – do klonowania obiektów
* zmienna arguments przechowuje wszystkie argumenty, nawet takie, których nie ma w definicji funkcji
* destructuring – symulacja nazywanych parametrów? – do klonowania tablic

- `Doczytaj`: destructuring

* Stos kontekstów wywołań
* Kontekst wywołania składa się z 3 rzeczy:  
* lexical environment / variable object (wszystkie zmienne widoczne w tej funkcji), 
* scope chain (mój  lexical env + lexical env wszystkich moich rodziców)
* this value
* tworzenie lexical env składa sięz 2 kroków:
* creation stage (widzi funkcję, tworzy sobie zmienną arguments, tworzy zmienne z parametrów, szuka deklaracji funkcji i tworzy wskazniki na te funkcje, potem szuka deklaracji zmiennych i tworzy zmienne, ale bez wartości (na razie są undefined)
* execution stage (przypisuje wartości do zmiennych)

- `Doczytaj`: Domknięcia !

* Const vs let
* z defaultu używamy const
* Let przy pętli for
* Const nie może zmieniać referencji
* Var używamy tylko dla starych przeglądarek
* Jaka jest różnica między argumentami a parametrami? Parametry są w sygnaturze funkcji, a przy wywołaniu jak dostaje konkretną wartość przekazaną, to jest argument.
* arguments.length – ile funkcja ma argumentów
* foo.length – ile funkcja ma parametrów
* foo.name – nazwa funkcji
* Staraj się zawsze nazywać swoje funkcje, bo masz wtedy nazwę w stack trace!
* Jak się nazywa operator …
* Jeśli w deklaracji funkcji – rest operator np. Function foo(…args){}
* Jeśli w wywołaniu funkcji – spread  np. Foo(…arr)
* THIS zachowuje się na jeden z 4 przypadków !?!?
* wywołanie w globalnym scopie
* wywołanie metody na obiekcie
* wywołanie metodą call z ustawionym elementem, który będzie traktowany jak this
* wywołanie operatorem new
* operator bind zwraca funkcje, która robi to samo, co nasza normalna funkcja, tylko ma zaszytego thisa

## Arrow functions

* `function f() {return 1;}`
* `() => 1`
* `() => {}` nic nie zwraca
* `() =>({})` zwraca pusty obiekt
* `x => x` funkcja zwraca to, co dostała na wejściu
* `(x,y) => x+y`
* `function fn(…args) {}` zamieniamy na `(…args) => args`
* `(x,y) => {return x+y}`
* `() => ({a: 1})`
* This i arguments zachowują się inaczej przy arrow functions, wewnąrz arrow func ma leksykalnego thisa, traktuje thisa jak kazdą inną zmienną, jak nie widzi jej w scopie to idzie do rodzica i tam sprawdza czy jest w scopie
* Kiedy nie używać arrow functions? Funkcji prototypu? Jeżeli coś wcześniej bazowało na thisie, nie przepisujemy na arrow functions.
* Arguments też jest leksykalne, też będzie ich szukał w scopie prototypów

* Good practice: wyciąganie nieprzewidywalnych funkcji do parametrów, aby dało sięto testować
* Najpierw klonujemy obiekt Object.assign({}, myObj), a potem dopiero robimy na nim operacje
* W programowaniu funkcyjnym :
* nie ma nulli, są opcje, nie ma wyjątków, 
* zwracamy obiekt, który zwraca błąd
* są funkcje, które zwracającą funkcje (higher order functions)
* referencial transparency

- `Doczytaj`: szczegóły i rożnice między call i apply
- `Doczytaj`: underscore.js
- `Doczytaj`: lodash.js
- `Doczytaj`: funkcja every na array

* Array.isArray()
* `undefined++` daje NaN

## Asynchroniczność

* Concurrent vs parallel
* Concurrent – 2 kolejki do 1 automatu
* Parallel – 2 kolejki do 2 automatów
* JS jest concurrent
* Single threaded event loop //jednowątkowa pętla zdarzeń
* Callback
* 4 problemy z promisami
* Zagnieżdzenia,
* Wywołanie wielokrotne ?? //nie wiemy, ile razy ten obcy kod wywoła callback
* Koordynacja,
* Zalgo
* Promise – obiekt, który ma na sobie funkcję then, funkcja w then wywoła się, gdy dane zostaną dostarczone (obietnica przyszłej wartości)
* `function errback(err, data) {}`

```
Ajax()
    .then(function(data) {})
    .catch(function(err){})
```
* `function fakeAjax(){return Promise.resolve(……);}`
* `function fakeErrorAjax(){return Promise.reject(“500 Server Error”)}`
* `new Promise(function(resolve, reject){})`
* Nie ważne, co zwracamy z promisa, i tak się to opakuje w promisa
* `Promise.all([p1,p2,p3]).then(function([p1res,p2res,p3res]){}).catch(function(err){});`
* Nigdy to, co mamy w then nie wykona się więcej niż 1 raz
* Zamiast: `(url => fetch(url))` robimy `(fetch)`
* Zamiast `done()` możemy zwracać promisa i framework testowy będzie wiedział, o co chodzi
* Czego promisy nie rozwiązują??? Nie dają Synchronicznie wyglądającego kodu asynchronicznego
```
async function
var data = await fetch();
```
* wtedy łapiemy wyjątki zwykłym try catch

# API

[https://gist.github.com/kwasniew/585466f9b41643dd8cf032610edb297d](https://gist.github.com/kwasniew/585466f9b41643dd8cf032610edb297d)
* npm i express
* Middleware
* Możemy rzucić wyjątek albo przekazać go jako parametr w metodzie `next()`
* Aby obsłużyć 404, wpinamy middleware po wszystkich handlerach get() – bo nie znalazł odpowiedniego adresu routingu wcześniej, dlatego zgłasza 404 not found
* Kolejność: najpierw middlewary generyczne, logika biznesowa, obsługa 404, obsługa innych błędów

- `Doczytaj`: next()

* Controller: request, DB, response, po 1 linii a resztę  wywal do innych plików
* Repository
* Pattern: Post – Redirect – get
* Na posta odpowiadamy  Redirect 302
* Albo async, albo done, nie obydwa
* Wartości proste porównuj equalem, a obiekty deppequalem
* Zależności: Routes -> Controller -> Repository

# FRONTEND

* `document.querySelector`
* Biblioteka hyperap
```
npm i http-server –g
http-server .
```
* stan, akcje, widok  //widok jest funkcją stanu
* tagged template literals:	 
```
nazwafunkcji`<div></div>`
```
* npm + bundler (przerabia require) + transpiler (wspiera stare ficzery ☺)
* Bundlery:
    + browserify (robi jeden wieeelki plik)
    + watchify (nasłuchuje też na zmiany)
    + parcel bundler – nowe, automatycznie zaciąga pakiety ?
* Transpilator
    + Babel
* source map – mapuje pliki kodu, inny dla przeglądarki, inny do debugowania
* webpack ma source mapy dla dependency
* Akcje to funkcje, które zwracają nową wartość stanu 


* XMLHttpRequest
* JSONHTTP/FileRequestResponse
* Transpilowanie asynca jest kosztowne, więc async na backendzie, promisy na frontendzie
* [https://polyfill.io/v2/docs/](https://polyfill.io/v2/docs/)
* [http://youmightnotneedjquery.com/#json](http://youmightnotneedjquery.com/#json)
* PWA – progressive web apps
* `localStorage.getItem()` / `localStorage.setItem()`
* wzorzec dekorator
* load time performance
* run time performance
* html + css + js mają mieć razem nie więcej niż 130 KB, inaczej na słabym sprzęcie na mobilnych nie załaduje się poniżej 5s
* nie więcej niż 16 milisekund na zdarzenie w run time performance


