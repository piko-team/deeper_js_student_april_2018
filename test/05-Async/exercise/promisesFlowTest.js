const assert = require('assert');

// this can be some third party library that we don't know the source of
function fetch(url, cb) {
    return new Promise(function(resolve, reject) {
        setTimeout(function () {
            resolve('data from ' + url);
        }, Math.random() * 2);
    });
}

describe('Callbacks flow', function () {
    it.only('sequential', function (done) {
        var results = [];

        fetch('http://example1.com')
            .then(function(data){
                results.push(data);
                return data;
            })
            .then(function(data){
                return fetch('http://example2.com');
            })
            .then(function(data){
                results.push(data);
                return data;
            })
            .then(function(data){
                return fetch('http://example3.com');
            })
            .then(function(data){
                results.push(data);
                return data;
            })
            .then(function(data){
                assert.deepEqual(results, ['data from http://example1.com', 'data from http://example2.com', 'data from http://example3.com']);
                return data;
            })
            .then(function(data){
                return done();
            });
    });

    it.only('parallel', function (done) {
        var urls = ['http://example1.com', 'http://example2.com', 'http://example3.com'];

        Promise.all(urls.map(fetch))
            .then(function(results){
                assert.deepEqual(results, ['data from http://example1.com', 'data from http://example2.com', 'data from http://example3.com']);
                done();
            });
    });
    

});