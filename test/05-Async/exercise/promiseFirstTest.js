var assert = require('assert');

describe('promise first utility', function () {
    before(function () {
        if (!Promise.first) {
            // TODO: your code goes here
        }
    });

    after(function () {
        delete Promise.first;
    });


    it.skip('Promise.first fulfills as soon as the first promise fulfills and ignores rejections', function (done) {
        Promise.first([Promise.reject(1), Promise.resolve(2)]).then(function (result) {
            assert.equal(result, 2);
            done();
        });
    });

    it.skip('Promise.first rejects when no promise fulfills', function (done) {
        Promise.first([Promise.reject(1), Promise.reject(2)]).catch(function (result) {
            assert.equal(result, 'all promises rejected');
            done();
        });
    });

});