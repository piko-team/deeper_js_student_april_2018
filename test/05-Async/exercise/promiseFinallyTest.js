var assert = require('assert');

describe('Promise.finally()', function () {

    before(function () {
        if (typeof Promise.prototype.finally !== 'function') {
            // TODO: your code goes here
        }
    });

    after(function () {
        delete Promise.prototype.finally;
    });

    it.skip('triggers on resolve', function (done) {
        Promise.resolve('some resource created').finally(function () {
            done();
        });
    });

    it.skip('triggers on reject', function (done) {
        Promise.reject('resource creation failed').finally(function () {
            done();
        });
    });

    it.skip('does not affect the fulfillment result', function (done) {
        Promise.resolve('some resource created').finally(function () {

        }).then(function (value) {
            assert.equal(value, 'some resource created');
            done();
        });
    });

    it.skip('does not affect the rejection reson', function (done) {
        Promise.reject('resource creation failed').finally(function () {

        }).catch(function (error) {
            assert.equal(error, 'resource creation failed');
            done();
        });
    });

    it.skip('propagates own failure', function (done) {
        Promise.resolve('some resource cereated').finally(function () {
            throw 'error in finally';
        }).catch(function (error) {
            assert.equal(error, 'error in finally');
            done();
        });
    });

    it.skip('own failure overrides original failure', function (done) {
        Promise.reject('resource creation failed').finally(function () {
            return Promise.reject('error in finally');
        }).catch(function (error) {
            assert.equal(error, 'error in finally');
            done();
        });
    });
});
