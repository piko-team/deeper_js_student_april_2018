const assert = require('assert');

// this can be some third party library that we don't know the source of
function fetch(url, cb) {
    setTimeout(function () {
        cb('data from ' + url);
    }, Math.random() * 10);
}

describe('Callbacks flow', function () {
    it('sequential', function (done) {
        var results = [];

        fetch('http://example1.com', function (data) {
            results.push(data);
            fetch('http://example2.com', function (data) {
                results.push(data);
                fetch('http://example3.com', function (data) {
                    results.push(data);
                    assert.deepEqual(results, ['data from http://example1.com', 'data from http://example2.com', 'data from http://example3.com']);
                    done();
                });
            });
        });
    });

    it('parallel', function (done) {
        var results = [];

        var urls = ['http://example1.com', 'http://example2.com', 'http://example3.com'];

        urls.forEach(function(url, idx) {
            fetch(url, function(data) {
                results.push({data, idx});

                if(results.length === urls.length) {
                    assert.deepEqual(results.sort(x=>x.idx).map(x=>x.data), ['data from http://example1.com', 'data from http://example2.com', 'data from http://example3.com']);
                    done();
                }
            });
        });        
    });

});