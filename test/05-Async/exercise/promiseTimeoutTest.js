var assert = require('assert');

/* eslint-disable no-unused-vars */
function timeout(ms, promise) {
    // TODO: your code goes here
}
/* eslint-enable no-unused-vars */

describe('promise timeout utility', function () {
    it.skip('times out when promise does not resolve', function (done) {
        timeout(1, new Promise(() => {})).catch(function(err) {
            assert.equal(err, 'Timeout');
            done();
        });
    });

    it.skip('does not time out when promise resolves earlier', function (done) {
        timeout(100, Promise.resolve('data')).then(function(data) {
            assert.equal(data, 'data');
            done();
        });
    });

    it.skip('does not time out when promise resolves earlier', function (done) {
        timeout(100, Promise.reject('error')).catch(function(err) {
            assert.equal(err, 'error');
            done();
        });
    });
});
