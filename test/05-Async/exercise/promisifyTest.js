var assert = require('assert');
var fs = require('fs');

/* eslint-disable no-unused-vars */
function promisifyNode(fn, ...args) {
    // TODO: your code goes here
}
/* eslint-enable no-unused-vars */

describe('promisify utility for node APIs', function () {
    it('specific', function () {
        return new Promise(function (resolve, reject) {
            fs.readFile(__dirname + '/promisifyTest.js', 'UTF-8', function (err, data) {
                if (err) {
                    reject(err);
                } else {
                    resolve(data);
                }
            });
        }).then(
            function (data) {
                assert.ok(data.includes('promises'));
            }
        );

    });

    it.skip('generic', function () {
        return promisifyNode(fs.readFile, __dirname + '/promisifyTest.js', 'UTF-8').then(function (data) {
            assert.ok(data.includes('promises'));
        });
    });

});