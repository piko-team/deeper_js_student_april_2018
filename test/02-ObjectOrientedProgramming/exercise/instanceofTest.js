var assert = require('assert');

function Person(name) {
    this.name = name;
}

function INSTANCEOF(obj, constructor) {
<<<<<<< HEAD
    if (obj.__proto__ == null)
        return false;
    else if (obj.__proto__ === constructor.prototype)
        return true;
    else return INSTANCEOF(obj.__proto__, constructor);
=======
    if(obj.__proto__ === null) {
        return false;
    }
    if(obj.__proto__ === constructor.prototype) {
        return true;
    }
    return INSTANCEOF(obj.__proto__, constructor);
>>>>>>> 4608beb5922c92e9ac610e487dde47b578b78c81
}

describe('INSTANCEOF', function () {
    it('original implementation verifies prototype chain', function () {
        var p = new Person('Mateusz');

        assert.ok(p instanceof Person);
        assert.ok(p instanceof Object);
        assert.ok(!(p instanceof Array));
    });

    it('on self', function () {
        var p = new Person('Mateusz');

        assert.ok(INSTANCEOF(p, Person));
    });

    it('on own parent', function () {
        var p = new Person('Mateusz');

        assert.ok(INSTANCEOF(p, Object));
    });

    it('on another parent', function () {
        var p = new Person('Mateusz');

        assert.ok(!INSTANCEOF(p, Array));
    });
});
