const assert = require('assert');

/* eslint-disable no-unused-vars */
const book = {
    'items': [
        {
            'volumeInfo': {
                'title': 'JavaScript',
                'subtitle': 'The Definitive Guide',
                'authors': [
                    'David Flanagan'
                ]
            }
        }
    ]
};
/* eslint-enable no-unused-vars */

describe('Destructuring', function () {
    it('from a real world data structure', function () {
<<<<<<< HEAD

        // TODO: write your destructuring selector here
        // let { 
        //     items: [{
        //         volumeInfo: { 
        //             authors: [
        //                 author
        //             ] 
        //         } 
        //     }] 
        // } = book; 

        // Copied JSON and removed unecessary properties.
        let {
            'items': [
                {
                    'volumeInfo': {
                        'authors': [
                            author
                        ]
                    }
                }
            ]
        } = book;

        /* eslint-disable no-undef */
=======
        let {
            items: [
                {
                    volumeInfo: {
                        authors: [author]
                    }
                }
            ]
        } = book; // default arrays are usually a good idea

>>>>>>> 4608beb5922c92e9ac610e487dde47b578b78c81
        assert.equal(author, 'David Flanagan');
    });

});