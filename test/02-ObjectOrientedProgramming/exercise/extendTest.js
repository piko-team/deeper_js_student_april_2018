var assert = require('assert');

<<<<<<< HEAD
/* eslint-disable no-unused-vars */
function extend(target, source) {
    Array.from(arguments).forEach(arg=>{
        Object.keys(arg).forEach(key => {
            target[key] = arg[key];
        });
    });
=======
function extend(target, ...sources) {
    for (let i = 0; i < sources.length; i++) {
        Object.keys(sources[i]).forEach(function (key) {
            target[key] = sources[i][key];
        });
    }
>>>>>>> 4608beb5922c92e9ac610e487dde47b578b78c81
    return target;
}

// function extend(target) {
//     for (let i = 1; i < arguments.length; i++) {
//         const source = arguments[i];
//         Object.keys(source).forEach(function (key) {
//             target[key] = source[key];
//         });
//     }
//     return target;
// }

describe('Useful utility: extend, mixin, assign', function () {
    it('custom extend should add new properties', function () {
        var target = {name: 'Mateusz'};
        var source = {age: 32};

        var result = extend(target, source);

        assert.ok(result === target);
        assert.deepEqual(result, {name: 'Mateusz', age: 32});
    });

    it('custom extend should overwrite existing properties', function () {
        var target = {name: 'Mateusz'};
        var source = {name: 'Kate'};

        var result = extend(target, source);

        assert.deepEqual(result, {name: 'Kate'});
    });

    it('custom extend should copy only own properties', function () {
        var target = {name: 'Mateusz'};
        var prototype = {species: 'human'};
        var source = Object.create(prototype);

        var result = extend(target, source);

        assert.deepEqual(result, {name: 'Mateusz'});
    });

    it('custom extend should ignore empty source', function () {
        var target = {name: 'Mateusz'};

        var result = extend(target);

        assert.deepEqual(result, {name: 'Mateusz'});
    });

    it('custom extend should support multiple sources', function () {
        var target = {name: 'Mateusz'};
        var source1 = {age: 20, job: 'programmer'};
        var source2 = {age: 32, title: 'Mr'};

        var result = extend(target, source1, source2);

        assert.deepEqual(result, {name: 'Mateusz', age: 32, job: 'programmer', title: 'Mr'});
    });

    it('custom extend should ignore nonenumerable properties', function () {
        var target = {name: 'Mateusz'};
        var source = Object.defineProperty({}, 'age', {
            enumerable: false
        });

        var result = extend(target, source);

        assert.deepEqual(result, {name: 'Mateusz'});
    });

    it('custom extend copies getters as data properties', function () {
        var target = {name: 'Mateusz'};
        var source = {
            get age() {
                return 32;
            }
        };
        var result = extend(target, source);

        assert.deepEqual(Object.getOwnPropertyDescriptor(result, 'age').value, 32);
        assert.deepEqual(Object.getOwnPropertyDescriptor(result, 'age').get, undefined);
    });

    it('Object.assign advanced example', function () {
        var target = {name: 'Mateusz'};
        var prototype = {species: 'human'};
        var source = Object.create(prototype);
        source.age = 32;
        source.name = 'Kate';

        var result = extend(target, source);

        assert.ok(result === target);
        assert.deepEqual(result, {name: 'Kate', age: 32});
    });

    // below you'll find some examples of ES6 Object.assign. Use them as a reference
    it('Object.assign with null source', function () {
        var target = {name: 'Mateusz'};
        var result = Object.assign(target);

        assert.deepEqual(result, {name: 'Mateusz'});
    });

    it('Object.assign with multiple sources', function () {
        var target = {name: 'Mateusz'};
        var source1 = {age: 20, job: 'programmer'};
        var source2 = {age: 32, title: 'Mr'};

        var result = Object.assign(target, source1, source2);

        assert.deepEqual(result, {name: 'Mateusz', age: 32, job: 'programmer', title: 'Mr'});
    });

});