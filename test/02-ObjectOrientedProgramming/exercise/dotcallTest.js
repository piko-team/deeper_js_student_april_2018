var assert = require('assert');

function Person(name) {
    this.name = name;
}

Person.prototype.sayHiTo = function (otherName) {
    return 'Hi ' + otherName + ', I am ' + this.name;
};

var DOT = function (obj, prop) {
    if (obj.hasOwnProperty(prop)) {
        return obj[prop]; // imagine this one can't walk up the proto chain
    } else if (Object.getPrototypeOf(obj)) {
        return DOT(Object.getPrototypeOf(obj), prop);
    } else {
        return undefined;
    }
};

var DOTCALL = function (obj, prop, args) {
    var fn = DOT(obj, prop);
<<<<<<< HEAD
    if (typeof fn === 'function')
        return fn.apply(obj, args);
    throw new Error(`TypeError: ${prop} is not a function`);
=======

    if (typeof fn === 'function') {
        return fn.apply(obj, args);
    } else {
        throw new TypeError(prop + ' is not a function');
    }
>>>>>>> 4608beb5922c92e9ac610e487dde47b578b78c81
};

describe('DOTCALL and DOT', function () {
    it('calls function', function () {
        var p = new Person('Mateusz');
        var result = p.sayHiTo('Kate');
        assert.equal(result, 'Hi Kate, I am Mateusz');
    });

    it('DOT performs lookup', function () {
        var p = new Person('Mateusz');

        assert.equal(DOT(p, 'name'), 'Mateusz');
    });

    it('DOTCALL calls function', function () {
        var p = new Person('Mateusz');
        var result = DOTCALL(p, 'sayHiTo', ['Kate']);

        assert.equal(result, 'Hi Kate, I am Mateusz');
    });

    it('DOTCALL throws error on non callable invocation', function () {
        var p = new Person('Mateusz');

        assert.throws(() => {
            DOTCALL(p, 'name');
        }, /TypeError: name is not a function/);
    });

    it('DOTCALL throws error on nonexistent function invocation', function () {
        var p = new Person('Mateusz');

        assert.throws(() => {
            DOTCALL(p, 'doSth');
        }, /TypeError: doSth is not a function/);
    });
});