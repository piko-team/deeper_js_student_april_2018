var assert = require('assert');

/* eslint-disable no-unused-vars */
function typeofObject(obj) {
    /* TODO: your code goes here */
}
/* eslint-enable no-unused-vars */

describe('typeof object', function () {
    it.skip('objects', function () {
        assert.equal(typeofObject(new Date()), 'date');
        assert.equal(typeofObject(new String('a')), 'string');
    });

    it.skip('null', function () {
        assert.equal(typeofObject(null), 'null');
    });

    it.skip('primitive', function () {
        assert.equal(typeofObject(1), 'number');
    });

});