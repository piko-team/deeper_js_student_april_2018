var assert = require('assert');

var parent = {
    species: 'human'
};
Object.defineProperty(parent, 'hidden', {
    enumerable: false
});

var child = {
    name: 'Mateusz'
};
Object.defineProperty(child, 'age', {
    enumerable: false
});

Object.setPrototypeOf(child, parent);

/* eslint-disable no-unused-vars */
function getAllPropertyNames(o) {
    // TODO: your code goes here
}

function allKeys(o) {
    // TODO: your code goes here
}
/* eslint-enable no-unused-vars */


describe('objects keys and properties', function () {
    it.skip('getAllPropertyNames should return all property names from the proto chain', function () {
        var ownPropertyNames = Object.getOwnPropertyNames(child);
        var allProperties = getAllPropertyNames(child);

        assert.deepEqual(ownPropertyNames, ['name', 'age']);
        assert.deepEqual(allProperties.slice(0, 4), ['name', 'age', 'species', 'hidden']); // what are the other keys
    });

    it.skip('allKeys should return all enumerable keys from the proto chain', function () {
        var keys = Object.keys(child);
        var all = allKeys(child);

        assert.deepEqual(keys, ['name']);
        assert.deepEqual(all, ['name', 'species']);
    });

});