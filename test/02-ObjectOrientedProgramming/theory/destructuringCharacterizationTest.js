var assert = require('assert');

describe('Destructuring: ES6', () => {
    it('allows for object destructuring', () => {
        let person = {
            name: 'Mary',
            age: 32
        };

        // pattern how to do assignment
        let {name, age} = person;

        assert.equal(name, 'Mary');
        assert.equal(age, 32);
    });

    it('allows for destructuring assignment', () => {
        let person = {
            name: 'Mary',
            age: 32
        };
        let name = 'Kate';
        let age = 22;

        ({name, age} = person); // parenthesis = it's not a block statement
        // curly braces mean too many different things in JS

        assert.equal(name, 'Mary');
        assert.equal(age, 32);
    });

    it('allows for destructuring in function arguments', () => {
        let person = {
            name: 'Mary',
            age: 32
        };
        let name;
        let age;

        function process(value) {
            assert.strictEqual(value, person);
        }

        process({name, age} = person);

        assert.equal(name, 'Mary');
        assert.equal(age, 32);
    });

    it('destructuring has implicit default value for nonexistent values', () => {
        let person = {
            name: 'Mary'
        };

        let {name, age} = person;

        assert.equal(name, 'Mary');
        assert.equal(age, undefined);
    });

    it('destructuring can have explicit default value for nonexistent values', () => {
        let person = {
            name: 'Mary'
        };

        let {name, age = 10} = person;

        assert.equal(name, 'Mary');
        assert.equal(age, 10);
    });

    it('destructuring allows for different local variable names', () => {
        let person = {
            name: 'Mary',
            age: 32
        };

        let { name: localName, age: localAge, gender: localGender = 'female' } = person;

        assert.equal(localName, 'Mary');
        assert.equal(localAge, 32);
        assert.equal(localGender, 'female');
    });

    it('destructuring works with nested objects', () => {
        let person = {
            name: 'Mary',
            address: {
                city: 'Krakow'
            }
        };

        let { address: { city }} = person;
        // I'd argue that person.address.city is more readable

        assert.equal(city, 'Krakow');
        assert.equal(typeof address, 'undefined');
    });

    it('allows for array destructuring', () => {
        let tdd = ['red', 'green', 'refactor'];

        let [color1, color2] = tdd; // this is not an array, it's a pattern. what does the assignment return?
        let [,,step] = tdd;

        assert.equal(color1, 'red');
        assert.equal(color2, 'green');
        assert.equal(step, 'refactor');
    });

    it('allows for array destructuring assignment', () => {
        let tdd = ['red', 'green', 'refactor'];
        let color1, color2;

        [color1, color2] = tdd;

        assert.equal(color1, 'red');
        assert.equal(color2, 'green');
    });

    it('allows to swap variables with destructuring assignment', () => {
        let a = 1, b = 2;

        [a, b] = [b, a];

        assert.equal(a, 2);
        assert.equal(b, 1);
    });

    it('array destructuring assignment can have default values', () => {
        let t = ['red'];
        let color1, color2;
        [color1, color2 = 'green'] = t;

        assert.equal(color1, 'red');
        assert.equal(color2, 'green');
    });

    it('array destructuring supports nesting', () => {
        let tdd = ['red', ['green', 'ignore'], 'refactor'];

        let [color1, [color2]] = tdd;

        assert.equal(color1, 'red');
        assert.equal(color2, 'green');
    });

    it('array destructuring supports rest items', () => {
        let tdd = ['red', 'green', 'refactor'];

        let [color1, ...steps] = tdd;

        assert.equal(color1, 'red');
        assert.deepEqual(steps, ['green', 'refactor']);
    });

    it('rest items can be used for cloning arrays', () => {
        var tdd = ['red', 'green', 'refactor'];
        var cloned_es5 = tdd.concat(/* with nothing */);
        let [...cloned_es6] = tdd;

        assert.deepEqual(cloned_es5, tdd);
        assert.deepEqual(cloned_es6, tdd);
        assert.ok(tdd !== cloned_es5);
        assert.ok(tdd !== cloned_es6);
    });

    it('supports mixed destructuring', () => {
        let person = {
            address: {
                home: {
                    city: 'Krakow'
                }
            },
            luckyNumbers: [7, 10]
        };

        let { address: { home: {city}}, luckyNumbers: [first]} = person;

        assert.equal(city, 'Krakow');
        assert.equal(first, 7);
    });

    // simulate named arguments - I use it a lot. order of args does not matter
    // if you have more than 2 args - use this pattern here
    // remember about default object
    it('destructuring makes function signatures more explicit', () => {
        function register(name, {age, email, gender = 'M'} = {}) {
            assert.equal(age, 32);
            assert.equal(gender, 'M');
            assert.equal(email, undefined);
        }

        register('Mary', {age: 32});
    });

});