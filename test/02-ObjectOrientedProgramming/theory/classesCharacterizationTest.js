var assert = require('assert');

// classes are just syntactic sugar on the top of behavior delegation of prototypes
describe('Classes: ES6', () => {
    it('introduces class declaration', function () {
        // it's not hoisted
        class Person {
            constructor(name) {
                this.name = name;
            }

            sayName() {
                return 'I am ' + this.name;
            }
        }

        var person = new Person('Mateusz');

        assert.equal(person.sayName(), 'I am Mateusz');
        assert.ok(person instanceof Person);
        assert.ok(person instanceof Object);
        assert.equal(typeof Person, 'function');
        assert.equal(typeof Person.prototype.sayName, 'function');
        assert.equal(Person.name, 'Person');
    });

    it('classes can extend from other classes', function () {
        class Rectangle {
            constructor(width, length) {
                this.width = width;
                this.length = length;
            }

            getArea() {
                return this.width * this.length;
            }
        }

        // extends can be any expression and that's why classes are not hoisted
        class Square extends Rectangle {
            constructor(length) {
                // in this context super means Rectangle
                super(length, length);
            }
        }

        var s = new Square(3);

        assert.equal(s.getArea(), 9);
        assert.ok(s instanceof Square);
        assert.ok(s instanceof Rectangle);
    });

    it('introduces class expressions', function () {
        var Person = class {
            constructor(name) {
                this.name = name;
            }

            sayName() {
                return 'I am ' + this.name;
            }
        };

        var person = new Person('Mateusz');

        assert.equal(person.sayName(), 'I am Mateusz');
        assert.equal(Person.name, 'Person');
    });

    it('introduces named class expressions', function () {
        var PersonAlias = class Person {
            constructor(name) {
                this.name = name;
            }

            sayName() {
                return 'I am ' + this.name;
            }
        };

        assert.equal(typeof Person, 'undefined');
        assert.equal(typeof PersonAlias, 'function');
    });

    it('classes are first class objects/functions', function () {
        function createObject(classDefinition) {
            return new classDefinition();
        }

        var obj = createObject(class {
            sayHi() {
                return 'Hi';
            }
        });

        assert.equal(obj.sayHi(), 'Hi');
    });

    it('allows to create singletons with immediately invoked constructor', function () {
        var person = new class {
            constructor(name) {
                this.name = name;
            }

            sayName() {
                return 'I am ' + this.name;
            }
        }('Mateusz');

        assert.equal(person.sayName(), 'I am Mateusz');
    });

    it('classes can have getters and setters', function () {
        class Person {
            get name() {
                return this._name.toUpperCase();
            }

            set name(text) {
                this._name = text.toLowerCase();
            }
        }

        var p = new Person();
        p.name = 'MATeusz';

        assert.equal(p.name, 'MATEUSZ');
    });

    it('classes can have computed member names', function () {
        var methodName = 'sayName';

        class Person {
            constructor(name) {
                this.name = name;
            }

            [methodName]() {
                return this.name;
            }
        }

        var me = new Person('Mateusz');
        assert.equal(me.sayName(), 'Mateusz');
    });

    it('classes can have static members', function () {
        class Person {
            constructor(name) {
                this.name = name;
            }

            // non statics are added to a prototype
            sayName() {
                return 'I am ' + this.name;
            }

            // statics are added to constructor function
            static create(name) {
                return new Person(name);
            }
        }

        var person = Person.create('Mateusz');

        assert.equal(person.sayName(), 'I am Mateusz');
    });

    it('derived class methods can shadow base class methods', function () {
        class Base {
            callMe() {
                return 'base';
            }
        }
        class Derived extends Base {
            callMe() {
                // in this context super means Base.prototype
                return super.callMe() + ' derived'; // super is write time/statically bound, unlike this - for performance reasons
            }
        }

        var d = new Derived();

        assert.equal(d.callMe(), 'base derived');
        assert.equal(Derived.prototype.callMe.apply({}, []), 'base derived');
    });

    it('static members are inherited', function () {
        class Base {
            static callMe() {
                return 'base';
            }
        }
        class Derived extends Base {
        }

        assert.equal(Derived.callMe(), 'base');
    });

    // it used to be very difficult pre ES6
    // don't subclass arrays, but subclass Errors this way
    it('unlike ES5 allows to inherit from built-ins', function () {
        class MyArray extends Array {
        }

        var a = new MyArray();
        a[0] = 'test';
        assert.equal(a.length, 1);

        a.length = 0;
        assert.equal(a[0], undefined);
    });

    // another example would be .stack preserving Error
    it('inheritance from built-ins preserves species', function () {
        class MyArray extends Array {
        }

        var a = new MyArray(1, 2, 3);
        var b = a.slice(1, 2);

        assert.ok((b instanceof MyArray));
    });

    it('classes cannot be called without new', function () {
        class MyClass {}

        assert.throws(() => {
            MyClass();
        }, /Class constructor MyClass cannot be invoked without 'new'/);
    });

    it('classes have default constructor', function () {
        class Foo {
            // default constructor
            // constructor() {}
            constructor(a, b) {
                this.a = a;
                this.b = b;
            }
        }
        class Bar extends Foo {
            // default constructor
            //constructor(...args) {
            //    super(...args);
            //}
        }
        var bar = new Bar('1', '2');

        assert.equal(bar.a, '1');
        assert.equal(bar.b, '2');
    });

    // Kyle Simpson - prototypes leak out in classes
    // Axel Rauschmayer - it should never bite you
    it('gotcha - classes still use linking mechanism underneath', function () {
        class NumberDispenser {
            constructor() {
                this.num = 10;
            }
            dispenseNumber() {
                return this.num;
            }
        }

        var d1 = new NumberDispenser();
        assert.equal(d1.dispenseNumber(), 10);

        NumberDispenser.prototype.dispenseNumber = function() {
            return 20;
        };

        var d2 = new NumberDispenser();
        assert.equal(d2.dispenseNumber(), 20);

        assert.equal(d1.dispenseNumber(), 20); // d1 is not a copy of a class but a live link to proto
    });

    it('gotcha - this is dynamic, super is static', function () {
        class Parent {
            whisper() {
                return 'psst';
            }
            shout() {
                return 'AAAA';
            }
        }
        class Child extends Parent {
            saySomething() {
                return this.shout() + ' ' + super.whisper();
            }
        }
        var randomObject = {
            whisper() {
                return 'hacked';
            },
            shout() {
                return 'hacked';
            }
        };

        var c = new Child();
        assert.equal(c.saySomething(), 'AAAA psst');

        assert.equal(c.saySomething.call(randomObject), 'hacked psst'); // super is static, this is dynamic
    });
});