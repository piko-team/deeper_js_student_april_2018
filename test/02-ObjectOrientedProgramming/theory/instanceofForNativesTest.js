var assert = require('assert');

// objects w/o classes. classes - objects are copies. prototype - links
describe('Javascript', function () {
    it('has different object types aka natives', function () {
        assert.equal(typeof {}, 'object'); // Object
        assert.equal(typeof [], 'object'); // Array, WAT
        assert.equal(typeof new Date(), 'object'); // Date
        assert.equal(typeof /.*/, 'object'); // RegExp
        assert.equal(typeof function () {
        }, 'function'); // Function, special case
        assert.equal(typeof String('a'), 'string');
        assert.equal(typeof new String('a'), 'object'); // WTF

        assert.ok({} instanceof Object);
        assert.ok([] instanceof Array);
        assert.ok(new TypeError('abc') instanceof Error);
        assert.ok(Array.isArray([])); // works across frames
        assert.ok(new Date() instanceof Date);
        assert.ok(/.*/ instanceof RegExp);
        assert.ok(new Boolean(true) instanceof Boolean);
        assert.ok(function () {} instanceof Function);
        assert.ok(new String('hello') instanceof String);
        assert.equal('hello' instanceof String, false); // WTF
        assert.equal(NaN instanceof Number, false);
    });

    it('objects have internal [[Class]] property', function() {
        assert.equal(Object.prototype.toString.call([]), '[object Array]'); // used to implement Array.isArray
        assert.equal(Object.prototype.toString.call({}), '[object Object]');
        assert.equal(Object.prototype.toString.call(true), '[object Boolean]');
        assert.equal(Object.prototype.toString.call(new Error()), '[object Error]');
        assert.equal(Object.prototype.toString.call(function() {}), '[object Function]');
        assert.equal(Object.prototype.toString.call(JSON), '[object JSON]');
        assert.equal(Object.prototype.toString.call(Math), '[object Math]');
        assert.equal(Object.prototype.toString.call(1), '[object Number]');
        assert.equal(Object.prototype.toString.call(/a/), '[object RegExp]');
        assert.equal(Object.prototype.toString.call(''), '[object String]');

        // less performant but less typing
        assert.equal({}.toString.call(1), '[object Number]');
    });

});