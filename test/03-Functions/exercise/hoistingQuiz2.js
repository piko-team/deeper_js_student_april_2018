// what will the code look like after hoisting?
console.log(a);
console.log(b);
var a = b;
var b = 2;
console.log(b);
console.log(a);

// translates to this after hoisting
// TODO: write your code here
// var a;
// var b;
// console.log(a);
// console.log(b);
// a = b;
// b = 2;
// console.log(b);
// console.log(a);


var a = undefined;
var b = undefined;

a = b;
b = 2;