const assert = require('assert');

// TODO: write code that will make the tests green

// new, this .prototype
function Programmer1() {
    this._knownsLangs = [];
}
Programmer1.prototype.isPragmatic = function isPragmatic(){
    return this._knownsLangs.length > 2;
};
Programmer1.prototype.learnNewLanguage = function learnNewLanguage(lang){
    this._knownsLangs.push(lang);
};

// new, this
class Programmer2 {
    constructor(){
        this._knownsLangs = [];
    };

    isPragmatic(){
        return this._knownsLangs.length > 2;
    };

    learnNewLanguage(lang) {
        this._knownsLangs.push(lang);
    };
}

// 
var Programmer3 = {
    'init': function init(){ 
        _knownsLangs = [];
    },

    'learnNewLanguage': function learnNewLanguage(lang){
        _knownsLangs.push(lang);
    },

    'isPragmatic': function isPragmatic(){
        return _knownsLangs.length > 2;
    }
};

function createProgrammer() {
    let _knownsLangs = [];
    return {
        learnNewLanguage: function learnNewLanguage(lang){
            _knownsLangs.push(lang);
        },
        isPragmatic: function isPragmatic(){
            return _knownsLangs.length > 2;
        }
    };
}


describe('Comparing thisful and thisless style of programming', function () {
    it('thisful with prototype', function () {
        const programmer = new Programmer1();

        programmer.learnNewLanguage('Java');
        programmer.learnNewLanguage('Ruby');
        assert.ok(!programmer.isPragmatic());
        programmer.learnNewLanguage('Python');
        assert.ok(programmer.isPragmatic());
    });

    it('thisful with class', function () {
        const programmer = new Programmer2();

        programmer.learnNewLanguage('Java');
        programmer.learnNewLanguage('Ruby');
        assert.ok(!programmer.isPragmatic());
        programmer.learnNewLanguage('Python');
        assert.ok(programmer.isPragmatic());
    });

    // no class and .prototype allowed
    it('thisful with Object.create()', function () {
        const programmer = Object.create(Programmer3);
        programmer.init();

        programmer.learnNewLanguage('Java');
        programmer.learnNewLanguage('Ruby');
        assert.ok(!programmer.isPragmatic());
        programmer.learnNewLanguage('Python');
        assert.ok(programmer.isPragmatic());
    });

    // in this exercise you cannot use this
    // only functions and object literals are allowed
    it('thisless', function () {
        const programmer = createProgrammer();

        programmer.learnNewLanguage('Elm');
        programmer.learnNewLanguage('Clojure');
        assert.ok(!programmer.isPragmatic());
        programmer.learnNewLanguage('Haskell');
        assert.ok(programmer.isPragmatic());
    });
});