function foo() {
    var a = 'private variable';
    return function bar() {
        console.log(a);
    };
}

foo()();


//var globalExecutionContext = {
//    lexicalEnvironment: {
//        foo: pointer to function foo()
//    },
//    scopeChain: [globalExecutionContext.lexicalEnvironment]
//};
//
//var fooExecutionContext = {
//    lexicalEnvironment: {
//        TODO: your code goes here
//    },
//    scopeChain: [fooExecutionContext.lexicalEnvironment, globalExecutionContext.lexicalEnvironment]
//};
//
//var barExecutionContext = {
//    lexicalEnvironment: {
//
//    },
//    scopeChain: [TODO: your code goes here]
//};
