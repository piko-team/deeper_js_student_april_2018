// what will this code do?
var a = b();
var c = d();
console.log(a);
console.log(c);

function b() {
    return c;
}
var d = function() {
    return b();
};

// translates to
//
// function b() {
//     return c;
// }
// var a;
// var c;
// var d;
// a = b();
// c = d();
// console.log(a);
// console.log(c);
// d = function() {
//     return b();
// };

function b() { return c; }

var a = undefined;
var c = undefined;
var d = undefined;

a = b();
c = undefined;

console.log(a);
console.log(c);









