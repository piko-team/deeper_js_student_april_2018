const assert = require('assert');

function bind(originalFn, thisArg) {
    return function bindThis() {
        return originalFn.apply(thisArg, arguments);
    };
}

describe('Custom bind', function () {

    var person = {
        name: 'Mike',
        saySomething: function (a) {
            return this.name + a; // hint: set a breakpoint here
        }
    };

    var sayWithPersonBound = person.saySomething.bind(person);
    var customSayWithPersonBound = bind(person.saySomething, person);

    it('should mimic Object.bind()', function () {
        assert.equal(sayWithPersonBound('!'), 'Mike!');
        assert.equal(customSayWithPersonBound('!'), 'Mike!');

        var personHacked = {name: 'hacked name', saySomething: customSayWithPersonBound};

        assert.equal(personHacked.saySomething('!'), 'Mike!');
    });

    /* eslint-disable no-unused-vars */
    it('how should bind work together with a new operator?', function () {
        var o = new sayWithPersonBound('!');

        // TODO: uncomment correct assertion
        //assert.deepEqual(o, 'Mike!');
        //assert.deepEqual(o, 'undefined!');
        assert.deepEqual(o, {});
    });
    /* eslint-enable no-unused-vars */

});