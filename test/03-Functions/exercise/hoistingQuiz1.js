/* eslint-disable no-redeclare */

// creations - function and variable declarations hoisting, execution stage: variable assignments
var x = 2;
console.log(x); // ??
var x;
console.log(x); // ??

// TODO: write your code here
// var x;
// x = 2;
// console.log
// console.log




/* eslint-enable no-redeclare */