var Benchmark = require('benchmark');

var suite = new Benchmark.Suite;

// hoisted
function Programmer1() {
    this.languages = [];
}

Programmer1.prototype.learnNewLanguage = function (lang) {
    this.languages.push(lang);
};

Programmer1.prototype.isPragmatic = function () {
    return this.languages.length >= 3;
};

// not hoisted
// it's like macro for prototypes
class Programmer2 {
    constructor() {
        this.languages = [];
    }

    learnNewLanguage(lang) {
        this.languages.push(lang);
    }

    isPragmatic() {
        return this.languages.length >= 3;
    }
}

// closure - learnNewLanguage and isPragmatic will be invoked outside createProgrammer()
// and they will still remember state of the languages (languages is in their parent lexical scope)
function createProgrammer() {
    var languages = [];

    return {
        learnNewLanguage: function (lang) {
            languages.push(lang);
        },
        isPragmatic: function () {
            return languages.length >= 3;
        }
    };
}

var Programmer3 = {
    learnNewLanguage: function (lang) {
        this.languages.push(lang);
    },
    isPragmatic: function () {
        return this.languages.length >= 3;
    },
    init: function() {
        this.languages = [];
    }
};

// add tests
suite
    .add('prototype', function () {
        var programmer = new Programmer1();

        ['Clojure', 'Java', 'Go'].forEach(programmer.learnNewLanguage, programmer);

        programmer.isPragmatic();
    })
    .add('class', function () {
        var programmer = new Programmer2();

        ['Clojure', 'Java', 'Go'].forEach(programmer.learnNewLanguage, programmer);

        programmer.isPragmatic();
    })
    .add('closure', function () {
        var programmer = createProgrammer();

        ['Clojure', 'Java', 'Go'].forEach(programmer.learnNewLanguage);

        programmer.isPragmatic();
    })
    .add('OLOO', function () {
        var programmer = Object.create(Programmer3);
        programmer.init();

        ['Clojure', 'Java', 'Go'].forEach(programmer.learnNewLanguage, programmer);

        programmer.isPragmatic();
    })
    // add listeners
    .on('cycle', function (event) {
        console.log(String(event.target));
    })
    .on('complete', function () {
        console.log('Fastest is ' + this.filter('fastest').map('name'));
    })
    // run async
    .run({'async': true});


// if you have 90,000,000 ops/sec you're testing no op code
// beware JIT optimizations like dead code elimination
// those things above only matter if you create thousands of objects during critical path (not on load time)