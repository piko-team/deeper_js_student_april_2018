var assert = require('assert');

describe('Let, const: ES6', function () {
    /* eslint-disable no-constant-condition, no-unused-vars, no-undef */
    it('block bindings/variables can be declared with a let keyword', function () {
        assert.equal(functionVar, undefined); // functionVar is hoisted

        if (true) {
            // attach let vars to the block as opposed to the function
            let blockVar = 'block var';
            var functionVar = 'function var';
        }

        assert.deepEqual(functionVar, 'function var');
        assert.throws(() => {
            blockVar;
        }, /ReferenceError: blockVar is not defined/);
    });
    /* eslint-enable no-constant-condition, no-unused-vars, no-undef */

    /* eslint-disable no-constant-condition */
    it('let declaration can redeclare var in the same scope', function () {
        // var x = 10;
        // let x = 20; // this is SyntaxError

        var x = 10;
        if (true) {
            let x = 20; // block scope x shadowing function scope x

            assert.equal(x, 20);
        }
        assert.equal(x, 10);
    });
    /* eslint-enable no-constant-condition */

    /* eslint-disable no-const-assign */
    it('has const declarations', function () {
        const x = 10;

        assert.equal(x, 10);

        // not only in strict mode, but also in sloppy mode
        assert.throws(() => {
            x = 20;
        }, /TypeError: Assignment to constant variable/);
    });
    /* eslint-enable no-const-assign */

    /* eslint-disable no-constant-condition, no-unused-vars, no-undef */
    it('block bindings/variables can be declared with a const keyword', function () {
        if (true) {
            const blockVar = 'block var';
        }

        assert.throws(() => {
            blockVar;
        }, /ReferenceError: blockVar is not defined/);
    });
    /* eslint-enable no-constant-condition, no-unused-vars, no-undef */

    it('const declaration prevents binding modification but not value modification', function () {
        const car = {
            model: 'Mazda'
        };
        car.model = 'Porsche';

        assert.equal(car.model, 'Porsche');
    });

    // gotcha - let and const are hoisted, but error is thrown when accessed before initialized
    // TDZ is the region of a program, where a variable or a parameter cannot be accessed until it’s initialized
    it('let and const are not accessible before their declaration', function () {
        assert.equal(typeof blockVar, 'undefined');

        assert.throws(() => {
            // Temporal Dead Zone
            if(blockVar === 'block var') { throw 'should not go here'; }
            let blockVar = 'block var';
        }, /ReferenceError: blockVar is not defined/);
    });

    it('temporal dead zone is based on time not location', function () {
        /* eslint-disable no-constant-condition */
        if(true) {
            const f = function() {
                assert.equal(blockVar, 'temporal');
            };

            let blockVar = 'temporal';
            f();
        }
        /* eslint-enable no-constant-condition */
    });

    /* eslint-disable no-empty */
    it('block binding can be used in for loops', function () {
        for(var i = 0; i < 3; i++) {}
        for(let j = 0; j < 3; j++) {} // prefer this style

        assert.equal(i, 3);
        assert.equal(typeof j, 'undefined');
    });
    /* eslint-enable no-empty */

    // classic JS interview question
    it('block binding helps avoid confusion with function declarations in loops', function () {
        // single binding/storage space for variable i
        // and i is not scoped to a for loop but the whole test function
        var varResults = [];
        for(var i = 0; i < 3; i ++) {
            varResults.push(function() {
                return i;
            });
        }

        assert.equal(varResults[0](), 3);

        // canonical fix with IIFE
        var iifeResults = [];
        for(var k = 0; k < 3; k ++) {
            iifeResults.push((function(l) {
                return function() {
                    return l;
                };
            })(k));
        }

        assert.equal(iifeResults[0](), 0);


        // new j binding created for each iteration
        var letResults = [];
        for(let j = 0; j < 3; j ++) {
            letResults.push(function() {
                return j;
            });
        }

        assert.equal(letResults[0](), 0);
    });

    // this is how to achieve block scope in ES5
    it('exceptions in catch are block scoped', function () {
        var e = 'initial';
        try {
            throw 'exception';
        } catch (e) {
            assert.equal(e, 'exception');
        }

        /* eslint-enable no-undef */
        assert.equal(e, 'initial');
    });

});