var assert = require('assert');

describe('This binding: ', function () {
    it('every function while executing has a reference to its current invocation context, called this', function () {
        // forget everything you know about this from other languages. this is not lexical, call site matters

        function getName() {
            //'use strict';
            return this.name; // strict makes this undefined, otherwise global
        }

        function Person() {
            this.name = 'person4';
        }

        var o1 = {name: 'person1', sayName: getName};
        var o2 = {name: 'person2', sayName: o1.sayName};
        var o3 = {name: 'person3'};

        // 1 - default binding. Lowest precedence. function form.
        assert.equal(getName(), undefined); // this is set to global or undefined (strict mode)

        // 2- implicit binding. method form
        assert.equal(o1.sayName(), 'person1'); // this is set to o1
        assert.equal(o2.sayName(), 'person2');

        // 3- explicit binding - call/apply form
        assert.equal(getName.call(o3), 'person3'); // this is set to o3

        // 4- new binding - constructor function call. Highest precedence. constructor form
        var f1 = new getName(); // this points to newly created f1
        assert.deepEqual(f1, {});
        var f2 = new Person(); // there's no class instantion here
        assert.equal(f2.name, 'person4');
    });



});