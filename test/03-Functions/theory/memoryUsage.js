function Programmer() {
    this.languages = [];
}

Programmer.prototype.learnNewLanguage = function (lang) {
    this.languages.push(lang);
};

Programmer.prototype.isPragmatic = function () {
    return this.languages.length >= 3;
};

function createProgrammer() {
    var languages = [];

    return {
        learnNewLanguage: function (lang) {
            languages.push(lang);
        },
        isPragmatic: function () {
            return languages.length >= 3;
        }
    };
}

let array = [];
for (let i = 0; i < 10000; ++i) {

    // TODO: uncomment one of the below to see the memory usage
    var programmer = createProgrammer();
    // var programmer = new Programmer();

    array.push(programmer);
}
console.log(process.memoryUsage().heapUsed / 1000000, 'MB');
console.log(array.length);