const assert = require('assert');

function partialLeft() {}
function partialRight() {}

function buildUrl(url, path, id) {
    return `${url}/${path}/${id}`;
}

describe('partial application', function () {
    it.skip('partial left variant', function () {
        const getOrderById = partialLeft(buildUrl, 'http://example.com', 'order');

        assert.equal(getOrderById(1), 'http://example.com/order/1');
    });

    it.skip('partial left variant 2', function () {
        const getOrderByPathAndId = partialLeft(buildUrl, 'http://example.com');

        assert.equal(getOrderByPathAndId('order', '1'), 'http://example.com/order/1');
    });

    it.skip('partial right variant', function () {
        const getFixedOrderByHost = partialRight(buildUrl, 'order', '2');

        assert.equal(getFixedOrderByHost('http://test.com'), 'http://test.com/order/2');
    });

});