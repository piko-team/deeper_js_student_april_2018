var assert = require('assert');
var curry = require('./curry');

describe('curry in action', function () {
    it.skip('practical example with ajax', function () {
        function ajax(userId) {
            return Promise.resolve('data for ' + userId);
        }
        // we can create function templates from the base function
        // strategically place things that will change often as last arguments
        const fetchUserData = function(ajax, userId) {
            return userId != null ? ajax(userId) : null;
        };

        const fetcher = curry(fetchUserData);

        // we make ajax based fetcher here
        const fetchById = fetcher(ajax);

        return fetchById('user123').then(function(data) {
            assert.equal(data, 'data for user123');
        });
    });
});