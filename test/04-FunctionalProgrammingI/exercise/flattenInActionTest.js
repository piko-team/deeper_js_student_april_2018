var assert = require('assert');

function flatten() {
    let result = [];
    for(let i = 0; i < arguments.length; i++) {
        let element = arguments[i];
        if (Array.isArray(element)) {
            element.forEach(e=>{
                flatten(e).forEach(x=>result.push(x));
            });
        } else if (!Array.isArray(element)) {
            result.push(element);
        }
    }
    return result;
}

// from: http://reactivex.io/learnrx/
var movieLists = [
    {
        name: 'Instant Queue',
        videos: [
            {
                'id': 70111470,
                'title': 'Die Hard',
                'boxarts': [
                    {width: 150, height: 200, url: 'http://cdn-0.nflximg.com/images/2891/DieHard150.jpg'},
                    {width: 200, height: 200, url: 'http://cdn-0.nflximg.com/images/2891/DieHard200.jpg'}
                ],
                'url': 'http://api.netflix.com/catalog/titles/movies/70111470',
                'rating': 4.0,
                'bookmark': []
            },
            {
                'id': 654356453,
                'title': 'Bad Boys',
                'boxarts': [
                    {width: 200, height: 200, url: 'http://cdn-0.nflximg.com/images/2891/BadBoys200.jpg'},
                    {width: 150, height: 200, url: 'http://cdn-0.nflximg.com/images/2891/BadBoys150.jpg'}

                ],
                'url': 'http://api.netflix.com/catalog/titles/movies/70111470',
                'rating': 5.0,
                'bookmark': [{id: 432534, time: 65876586}]
            }
        ]
    },
    {
        name: 'New Releases',
        videos: [
            {
                'id': 65432445,
                'title': 'The Chamber',
                'boxarts': [
                    {width: 150, height: 200, url: 'http://cdn-0.nflximg.com/images/2891/TheChamber150.jpg'},
                    {width: 200, height: 200, url: 'http://cdn-0.nflximg.com/images/2891/TheChamber200.jpg'}
                ],
                'url': 'http://api.netflix.com/catalog/titles/movies/70111470',
                'rating': 4.0,
                'bookmark': []
            },
            {
                'id': 675465,
                'title': 'Fracture',
                'boxarts': [
                    {width: 200, height: 200, url: 'http://cdn-0.nflximg.com/images/2891/Fracture200.jpg'},
                    {width: 150, height: 200, url: 'http://cdn-0.nflximg.com/images/2891/Fracture150.jpg'},
                    {width: 300, height: 200, url: 'http://cdn-0.nflximg.com/images/2891/Fracture300.jpg'}
                ],
                'url': 'http://api.netflix.com/catalog/titles/movies/70111470',
                'rating': 5.0,
                'bookmark': [{id: 432534, time: 65876586}]
            }
        ]
    }
];

var expectedResults = [
    {'id': 70111470, 'title': 'Die Hard', 'boxart': 'http://cdn-0.nflximg.com/images/2891/DieHard150.jpg'},
    {'id': 654356453, 'title': 'Bad Boys', 'boxart': 'http://cdn-0.nflximg.com/images/2891/BadBoys150.jpg'},
    {'id': 65432445, 'title': 'The Chamber', 'boxart': 'http://cdn-0.nflximg.com/images/2891/TheChamber150.jpg'},
    {'id': 675465, 'title': 'Fracture', 'boxart': 'http://cdn-0.nflximg.com/images/2891/Fracture150.jpg'}
];

function extractSmallBoxarts(array) {
    // TODO: your code goes here
    // use Array.map, Array.filter and your custom flatten or flatMap function
    // no index array access is allowed in this exercise

    let json = JSON.stringify(flatten(array));
    //console.log(json);

    return flatten(
        array.map(movieList => {
            return movieList.map(list=>{
                list.videos.map(video=>{
                    return video.boxarts
                        .filter(box=>box.width <= 150)
                        .map()
                });
            });
        })
    );
}

describe('flatten movies', function () {
    it.skip('from multiple movie lists', function () {
        assert.deepEqual(extractSmallBoxarts(movieLists), expectedResults);
    });

});