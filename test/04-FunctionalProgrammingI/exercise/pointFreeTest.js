var assert = require('assert');
var curry = require('./curry');

describe('refactoring to point-free style', function () {
    it.skip('example', function () {
        var filter = curry(function(predicate, array) {
            return array.filter(predicate);
        });

        var match = curry(function(regexp, string) {
            return string.match(regexp);
        });

        // TODO: refactor to remove array and string
        var filterDigits = function(array) {
            return filter(function(string) {
                return match(/^[0-9]+$/, string);
            }, array);
        };

        assert.deepEqual(filterDigits(['noise', '123', 'no1se', '4']), ['123', '4']);
    });
});