'use strict';
// use strict prevents null this to be replaced with global

var assert = require('assert');

describe('Array.reduce', function () {
    before(function() {
        if (typeof Array.prototype.reduceCustom !== 'function') {
            Array.prototype.reduceCustom = function(f, accu){
                if (this == null)
                    throw new Error(`TypeError: Array.prototype.reduce called on null or undefined`);
                if (this.length < 1)
                    throw new Error(`TypeError: Reduce of empty array with no initial value`);
                if (typeof f !== 'function')
                    throw new Error(`TypeError: ${f} is not a function`);

                let accumulator = this[0];
                let initialIndex = 1;
                if (accu != null) {
                    accumulator = accu;
                    initialIndex = 0;
                }
                                
                for(let i = initialIndex; i < this.length; i++){
                    accumulator = f(accumulator, this[i]);
                }
                return accumulator;
            };
        }
    });

    it('reduces elements in array', function () {
        var result = [1, 2, 3].reduceCustom((prev, next) => prev + next);
        assert.equal(result, 6);
    });

    it('allows to pass initial value', function () {
        var result = [1, 2, 3].reduceCustom((prev, next) => prev + next, 10);
        assert.equal(result, 16);
    });

    it('does not allow for en empty array', function () {
        assert.throws(() => {
            [].reduceCustom((prev, next) => prev + next);
        }, /TypeError: Reduce of empty array with no initial value/);
    });

    it('does not allow to reduce with non function', function () {
        assert.throws(() => {
            [1].reduceCustom({});
        }, /TypeError: .* is not a function/);
    });

    it('does not allow to reduce with null', function () {
        assert.throws(() => {
            [].reduceCustom.call(null, (prev, next) => prev + next);
        }, /TypeError: Array.prototype.reduce called on null or undefined/);
    });

    it('does not allow to reduce with undefined', function () {
        assert.throws(() => {
            [].reduceCustom.call(undefined, (prev, next) => prev + next);
        }, /TypeError: Array.prototype.reduce called on null or undefined/);
    });

    it('reduce can be used to implement any other transformation', function () {
        /* eslint-disable no-unused-vars */
        function map(array, fn) {
            return array.reduceCustom(function mapFunction(accumulator, current) {
                accumulator.push(fn(current));
                return accumulator;
            }, []);
        }
        /* eslint-enable no-unused-vars */

        assert.deepEqual(map([1, 2, 3], (x) => x * 2), [2, 4, 6]);
    });

    it('reduces elements in array - reference implementation', function () {
        var result = [1, 2, 3].reduce((prev, next) => prev + next);
        assert.equal(result, 6);

        result = [1, 2, 3].reduce((prev, next) => prev + next, 10);
        assert.equal(result, 16);

        assert.throws(() => {
            [].reduce((prev, next) => prev + next);
        }, /TypeError: Reduce of empty array with no initial value/);

        assert.throws(() => {
            [1].reduce({});
        }, /TypeError: #<Object> is not a function/);

        assert.throws(() => {
            [].reduce.call(null, (prev, next) => prev + next);
        }, /TypeError: Array.prototype.reduce called on null or undefined/);

        assert.throws(() => {
            [].reduce.call(undefined, (prev, next) => prev + next);
        }, /TypeError: Array.prototype.reduce called on null or undefined/);
    });

    after(function () {
        delete Array.prototype.reduceCustom;
    });

});