var assert= require('assert');

function pick() {
    // TODO: your code goes here
}


describe('pick', function () {
    it.skip('returns a subobject with selected fields', function () {
        var object = { 'a': 1, 'b': '2', 'c': 3 };

        var result = pick(object, 'a', 'c');

        assert.deepEqual(result, { 'a': 1, 'c': 3 });
    });
});