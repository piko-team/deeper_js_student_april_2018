var assert = require('assert');

function zip() {
    // TODO: your code goes here
}

describe('zip', function () {
    it.skip('selects values from multiple arrays', function () {
        assert.deepStrictEqual(zip(['a', 'b'], [1, 2]), [['a', 1], ['b', 2]]);
        assert.deepStrictEqual(zip(['a', 'b', 'c'], [1, 2]), [['a', 1], ['b', 2], ['c', undefined]]);
        assert.deepStrictEqual(zip(['a', 'b'], [1, 2, 3]), [['a', 1], ['b', 2]]);
        assert.deepStrictEqual(zip(['a'], [1], [true]), [['a', 1, true]]);
        assert.deepStrictEqual(zip(['a']), [['a']]);
    });
});