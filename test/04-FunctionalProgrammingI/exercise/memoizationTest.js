var assert = require('assert');

describe('Referential transparency allows for', function () {
    it.skip('memoization', function () {
        /* eslint-disable no-unused-vars */
        const memoized = (fn, keymaker = JSON.stringify) => {
            // TODO: your code goes here
        };
        /* eslint-enable no-unused-vars */

        var slowFibonacci = (n) =>
            n < 2
                ? n
                : slowFibonacci(n - 2) + slowFibonacci(n - 1);

        var fastFibonacci = memoized(function (n) {
            if (n < 2) {
                return n;
            }
            else {
                return fastFibonacci(n - 2) + fastFibonacci(n - 1);
            }
        });


        assert.equal(fastFibonacci(40), 102334155);
    });
});