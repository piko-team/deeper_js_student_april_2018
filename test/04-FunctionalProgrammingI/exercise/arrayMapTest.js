var assert = require('assert');

describe('Array.map', function () {
    before(function () {
        if (typeof Array.prototype.mapCustom !== 'function') {
            Array.prototype.mapCustom = function(fn, thisArgs){
                let result = [];

                for(let i = 0; i < this.length; i++)
                    result.push(fn.apply(thisArgs, [this[i], i]));
                return result;
            };
        }

    });

    it('maps each element in array', function () {
        var m1 = [1, 2, 3].mapCustom(e => e * 2);
        assert.deepEqual(m1, [2, 4, 6]);
    });

    it('passes index as second argument', function () {
        var m2 = [1, 2, 3].mapCustom((e, i) => i);
        assert.deepEqual(m2, [0, 1, 2]);
    });

    it('allows to pass custom this', function () {
        var o = {
            val: 2,
            multiply: function (x) {
                return this.val * x;
            }
        };
        var m3 = [1, 2, 3].mapCustom(o.multiply, o);
        assert.deepEqual(m3, [2, 4, 6]);
    });

    it('maps each element in array - reference implementation', function () {
        var m1 = [1, 2, 3].map(e => e * 2);
        assert.deepEqual(m1, [2, 4, 6]);

        var m2 = [1, 2, 3].map((e, i) => i);
        assert.deepEqual(m2, [0, 1, 2]);

        var o = {
            val: 2,
            multiply: function (x) {
                return this.val * x;
            }
        };
        var m3 = [1, 2, 3].map(o.multiply, o);
        assert.deepEqual(m3, [2, 4, 6]);
    });

    after(function () {
        delete Array.prototype.mapCustom;
    });

});