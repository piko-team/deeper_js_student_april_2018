var assert = require('assert');

describe('Array.find', function () {
    before(function() {
        if (typeof Array.prototype.findCustom !== 'function') {
            // TODO: your code goes here
        }
    });

    it.skip('finds first element by using custom predicate', function () {
        let numbers = [10, 20, 30, 40];

        assert.strictEqual(numbers.findCustom(n => n > 25), 30);
    });

    it.skip('returns undefined when nothing found', function () {
        let numbers = [10, 20, 30, 40];

        assert.strictEqual(numbers.findCustom(n => n > 45), undefined);
    });

    it.skip('predicate has to be a function', function () {
        let numbers = [10, 20, 30, 40];

        assert.throws(() => {
            numbers.findCustom(2);
        }, /2 is not a function/);
    });

    it.skip('cannot be called on null or undefined', function () {
        assert.throws(() => {
            [].findCustom.call(null);
        }, /TypeError: Array.prototype.findCustom called on null or undefined/);
    });


    it('finds first element by using custom predicate - reference implementation', function () {
        let numbers = [10, 20, 30, 40];

        assert.strictEqual(numbers.find(n => n > 25), 30);
        assert.strictEqual(numbers.find(n => n > 45), undefined);
        assert.throws(() => {
            numbers.find(2);
        }, /TypeError: 2 is not a function/);
        assert.throws(() => {
            [].find.call(null);
        }, /TypeError: Array.prototype.find called on null or undefined/);
    });

    after(function() {
        delete Array.prototype.findCustom;
    });

});