var assert = require('assert');

/* eslint-disable no-unused-vars */
function takeWhile(array, predicate) {
    let result = [];
    array.every(function checkAll(element){
        if (predicate(element))
        {
            result.push(element);
            return true;
        }
        return false;
    });
    return result;
}

function takeWhileAlt(array, predicate) {
    // TODO: your code goes here
}
/* eslint-enable no-unused-vars */

describe('takeWhile', function () {
    it('takes elements until predicate is true', function () {
        let result = takeWhile([1, 2, 3, 10, 11, 4], function (n) {
            return n < 10;
        });

        assert.deepEqual(result, [1, 2, 3]);
    });

    it.skip('implemented with every', function () {
        let result = takeWhileAlt([1, 2, 3, 10, 11, 4], function (n) {
            return n < 10;
        });

        assert.deepEqual(result, [1, 2, 3]);
    });
});