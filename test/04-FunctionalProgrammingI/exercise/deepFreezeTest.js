const assert = require('assert');

function deepFreeze() {
    // TODO: your code goes here
}

describe('deep freeze', function () {
    it.skip('should freeze simple object', function() {
        let o = {name: 'Kate'};

        let frozen = deepFreeze(o);
        frozen.name = 'Alice';

        assert.equal(frozen.name, 'Kate');
    });

    it.skip('should freeze frozen object', function() {
        let o = {name: 'Kate'};

        let frozen = deepFreeze(deepFreeze(o));
        frozen.name = 'Alice';

        assert.equal(frozen.name, 'Kate');
    });

    it.skip('should freeze nested object', function() {
        let o = {name: 'Kate', address: { city: 'Krakow'}};

        let frozen = deepFreeze(o);
        frozen.address.city = 'Warsaw'; // should fail silently in non-strict mode

        assert.equal(frozen.address.city, 'Krakow');
    });

    it.skip('should freeze nested array', function() {
        let o = {name: 'Kate', friends: ['Alice']};

        let frozen = deepFreeze(o);
        assert.throws(() => { frozen.friends.push('Bob'); }, /object is not extensible/);

        assert.equal(frozen.friends.length, 1);
    });

    it.skip('should freeze object with function', function () {
        let o = {test: function() {}};

        let frozen = deepFreeze(o);

        assert.deepEqual(frozen, o);
    });
});