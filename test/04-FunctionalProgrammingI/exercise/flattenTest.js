var assert = require('assert');

function flatten() {
    let result = [];
    for(let i = 0; i < arguments.length; i++) {
        let element = arguments[i];
        if (Array.isArray(element)) {
            element.forEach(e=>{
                flatten(e).forEach(x=>result.push(x));
            });
        } else if (!Array.isArray(element)) {
            result.push(element);
        }
    }
    return result;
}

function flatMap() {
    // TODO: your code goes here
}

describe('flatten', function () {
    it('should flatten nested arrays', function () {
        assert.deepEqual(flatten([]), []);
        assert.deepEqual(flatten([1]), [1]);
        assert.deepEqual(flatten([1, 2]), [1, 2]);
        assert.deepEqual(flatten([1, [2]]), [1, 2]);
        assert.deepEqual(flatten([1, [[2]]]), [1, 2]);
        assert.deepEqual(flatten([[1, [2, 3]], [[4, 5], 6, 7, [8, 9]]]), [1, 2, 3, 4, 5, 6, 7, 8, 9]);
    });

    it.skip('is often combined with map as flat map', function() {
        var languages = {backend: ['Java', 'Groovy', 'Clojure'], frontend: ['JavaScript', 'Elm'], scripting: ['Bash', 'Python']};
        var allLanguages = flatMap(['backend', 'frontend', 'scripting'], function(key) {
            return languages[key];
        });

        assert.deepEqual(allLanguages, ['Java', 'Groovy', 'Clojure', 'JavaScript', 'Elm', 'Bash', 'Python']);
    });
});