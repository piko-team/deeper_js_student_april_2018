var assert = require('assert');

var reverse = () => {
    // TODO: your code goes here
};

describe('reverse', function () {
    it.skip('function arguments', function () {
        function sum(arr, reduceFn) {
            return arr.reduce(reduceFn);
        }

        var sumReversed = reverse(sum);

        assert.equal(sumReversed((a, b) => a + b, [1, 2, 3]), 6);
    });
});