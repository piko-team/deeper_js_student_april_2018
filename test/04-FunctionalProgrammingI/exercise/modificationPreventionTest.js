const assert = require('assert');

describe('JS', function () {
    // remember const keyword is for variable reassignment prevention not for constant values
    it('allows to define constant value - strict mode', function () {
        'use strict';

        var o = {};
        Object.defineProperty(o, 'PI', {
            value: 3.14,
            writable: false,
            configurable: false
        });

        assert.throws(() => {
            o.PI = 3;
        }, /TypeError: Cannot assign to read only property/);
    });

    it('allows to define constant value - non-strict mode', function () {
        var o = {};
        Object.defineProperty(o, 'PI', {
            value: 3.14,
            writable: false,
            configurable: false
        });
        o.PI = 3;

        assert.equal(o.PI, 3.14);
    });

    it('allows to prevent object extension - strict mode', function () {
        'use strict';
        var o = {
            a: 1
        };
        Object.preventExtensions(o);

        assert.throws(() => {
            o.b = 2;
        }, /TypeError: Cannot add property b, object is not extensible/);
    });

    it('allows to prevent object extension - non-strict mode', function () {
        var o = {
            a: 1
        };
        Object.preventExtensions(o);

        o.b = 2;

        assert.strictEqual(o.b, undefined);
    });

    it('preventExtensions() does not affect existing properties', function () {
        'use strict';

        var o = {
            a: 1
        };
        Object.preventExtensions(o);

        o.a = 2;

        assert.equal(o.a, 2);
        assert.equal(Object.getOwnPropertyDescriptor(o, 'a').configurable, true);

        delete o.a;
        assert.equal(o.a, undefined);
    });

    it('allows to seal objects - strict mode', function () {
        'use strict';

        var o = {
            a: 1
        };
        Object.seal(o);

        o.a = 2;

        assert.throws(() => {
            delete o.a;
        }, /TypeError: Cannot delete property 'a'/);

        assert.throws(() => {
            o.b = 3;
        }, /TypeError: Cannot add property b, object is not extensible/);

        assert.equal(o.a, 2);
        assert.strictEqual(o.b, undefined);
        assert.equal(Object.getOwnPropertyDescriptor(o, 'a').configurable, false);
        assert.equal(Object.getOwnPropertyDescriptor(o, 'a').writable, true);
    });

    it('allows to seal objects - non-strict mode', function () {
        var o = {
            a: 1
        };
        Object.seal(o);

        o.a = 2;
        o.b = 3;

        assert.equal(o.a, 2);
        assert.strictEqual(o.b, undefined);
        assert.equal(Object.getOwnPropertyDescriptor(o, 'a').configurable, false);
        assert.equal(Object.getOwnPropertyDescriptor(o, 'a').writable, true);
    });

    it('allows to freeze objects - strict mode', function () {
        'use strict';

        var o = {
            a: 1
        };
        Object.freeze(o);

        assert.throws(() => {
            o.a = 2;
        }, /TypeError: Cannot assign to read only property/);
        assert.throws(() => {
            o.b = 3;
        }, /TypeError: Cannot add property b, object is not extensible/);

        assert.equal(o.a, 1);
        assert.strictEqual(o.b, undefined);
        assert.equal(Object.getOwnPropertyDescriptor(o, 'a').configurable, false);
        assert.equal(Object.getOwnPropertyDescriptor(o, 'a').writable, false);
    });

    it('allows to freeze objects - non-strict mode', function () {
        var o = {
            a: 1
        };
        Object.freeze(o);

        o.a = 2;
        o.b = 3;

        assert.equal(o.a, 1);
        assert.strictEqual(o.b, undefined);
        assert.equal(Object.getOwnPropertyDescriptor(o, 'a').configurable, false);
        assert.equal(Object.getOwnPropertyDescriptor(o, 'a').writable, false);
    });

    // constraint - can only use preventExtensions() and defineProperty()
    function customFreeze() {
        // TODO: your code goes here
    }

    it.skip('allows to freeze objects - custom freeze', function () {
        var o = {
            a: 1
        };
        o = customFreeze(o);

        o.a = 2;
        o.b = 3;

        assert.equal(o.a, 1);
        assert.strictEqual(o.b, undefined);
        assert.equal(Object.getOwnPropertyDescriptor(o, 'a').configurable, false);
        assert.equal(Object.getOwnPropertyDescriptor(o, 'a').writable, false);
    });
});