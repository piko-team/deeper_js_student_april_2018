var assert = require('assert');

var unary = () => {
    // TODO: your code goes here
};

describe('Unary', function () {
    it.skip('makes one argument functions', function () {
        var result = ['1', '2', '3'].map(parseInt);

        assert.ok(isNaN(result[1]));
        assert.ok(isNaN(result[2]));

        // unary decorated parseInt
        result = ['1', '2', '3'].map(unary(parseInt));

        assert.deepEqual(result, [1, 2, 3]);
    });
});