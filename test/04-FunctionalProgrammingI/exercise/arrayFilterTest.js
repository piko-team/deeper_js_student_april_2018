var assert = require('assert');

describe('Array.filter', function () {
    before(function() {
        if (typeof Array.prototype.filterCustom !== 'function') {
            Array.prototype.filterCustom = function(fn, thisArgs){
                let result = [];

                for(let i = 0; i < this.length; i++)
                    if (fn.apply(thisArgs, [this[i], i]))
                        result.push(this[i]);
                return result;
            };
        }
    });

    it('filters in each element in the array', function () {
        var f1 = [1, 2, 3].filterCustom(e => (e % 2 === 0));
        assert.deepEqual(f1, [2]);
    });

    it('allows to pass custom this', function () {
        var o = {
            val: 2,
            isEven: function (x) {
                return x % this.val === 0;
            }
        };
        var f2 = [1, 2, 3].filterCustom(o.isEven, o);
        assert.deepEqual(f2, [2]);

        assert.throws(() => {
            [1, 2, 3].filterCustom({});
        }, /TypeError/);
    });

    it('filters each element in array - reference implementation', function () {
        var f1 = [1, 2, 3].filter(e => (e % 2 === 0));
        assert.deepEqual(f1, [2]);

        var o = {
            val: 2,
            isEven: function (x) {
                return x % this.val === 0;
            }
        };
        var f2 = [1, 2, 3].filter(o.isEven, o);
        assert.deepEqual(f2, [2]);

        assert.throws(() => {
            [1, 2, 3].filter({});
        }, /TypeError/);
    });

    after(function () {
        delete Array.prototype.filterCustom;
    });

});