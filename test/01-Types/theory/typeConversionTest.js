var assert = require('assert');

describe('Type conversion options in JS', function () {

    it('string -> number', function () {
        var foo = '123';
        var baz = parseInt(foo, 10); // technically it's parse not coercion, it behaves like one
        assert.strictEqual(baz, 123);

        baz = Number(foo); // this is explicit coercion
        assert.strictEqual(baz, 123);

        baz = +foo; // is it explicit? explicit is a subjective term
        assert.strictEqual(baz, 123);

        // Number('12px') vs parseInt('12px')
    });

    it('* -> number', function () {
        assert.strictEqual(Number(''), 0); // the root of all coercion evil!
        assert.strictEqual(Number('-0'), -0); // not symmetrical with ToString
        assert.strictEqual(Number(' 009 '), 9);
        assert.strictEqual(Number('3.14'), 3.14);
        assert.strictEqual(Number('0.'), 0);
        assert.strictEqual(Number('.0'), 0);
        assert.ok(isNaN(Number('.')));
        assert.strictEqual(Number('0xa'), 10);
        assert.strictEqual(Number(false), 0); // could be NaN in a different lang design
        assert.strictEqual(Number(true), 1); // same as above
        assert.strictEqual(Number(null), 0);
        assert.ok(isNaN(Number(undefined))); // inconsistent with null, WAT

        // objects
        var o1 = {
            valueOf: function () {
                return 1;
            },
            toString: function () {
                return '2';
            }
        };

        var o2 = {
            toString: function () {
                return '2';
            }
        };

        assert.strictEqual(Number(o1), 1);
        assert.strictEqual(Number(o2), 2);

        // arrays - numberification of arrays is a weird concept - should be NaN in all cases in a better lang design
        assert.strictEqual(Number([]), 0);
        assert.strictEqual(Number(['']), 0);
        assert.strictEqual(Number([''].valueOf().toString()), 0); // behind the scenes
        assert.strictEqual(Number(['0']), 0);
        assert.strictEqual(Number(['0'].valueOf().toString()), 0);
        assert.strictEqual(Number(['-0']), 0);
        assert.strictEqual(Number([null]), 0);
        assert.strictEqual(Number([undefined]), 0);
        assert.strictEqual(Number([[[]]]), 0);
        assert.ok(isNaN(Number(['a'])));
        assert.ok(isNaN(Number([1, 2, 3]))); // WTF - why is it different?
    });

    it('number -> string', function () {
        var foo = 123;
        var baz = foo.toString(); // boxing to object wrapper with toString. It's implicit mechanism/ 123.toString() won't work why?
        assert.strictEqual(baz, '123');

        baz = String(123); // this is my preference
        assert.strictEqual(baz, '123');

        baz = 123 + ''; // this is implicit coercion that's very popular in the wild
        assert.strictEqual(baz, '123');
    });

    /* eslint-disable no-sparse-arrays */
    it('* -> string', function () {
        assert.strictEqual(String(null), 'null');
        assert.strictEqual(String(undefined), 'undefined');
        assert.strictEqual(String(true), 'true');
        assert.strictEqual(String(1.23), '1.23');
        assert.strictEqual(String(-0), '0'); // ToString spec says - please lie

        // arrays - WAT toString
        assert.strictEqual([].toString(), '');
        assert.strictEqual(String([]), '');
        assert.strictEqual([1, 2, 3].toString(), '1,2,3'); // where's the [] gone?
        assert.strictEqual(String([1, 2, 3]), '1,2,3');
        assert.strictEqual([, ,].toString(), ',');
        assert.strictEqual(String([, ,]), ','); // where's , gone?
        assert.strictEqual([null, undefined].toString(), ','); // why not ['null', 'undefined']?

        // object
        assert.strictEqual({}.toString(), '[object Object]'); // not very useful
        assert.strictEqual({a: 2}.toString(), '[object Object]'); // not very useful

        // function stringification is a wild wild west

        assert.throws(() => {
            String(Object.create(null));
        }, /TypeError/);
    });
    /* eslint-enable no-sparse-arrays */

    it('JSON.stringify', function () {
        assert.equal(JSON.stringify(null), 'null');
        assert.equal(JSON.stringify(undefined), undefined);
        assert.equal(JSON.stringify(function () {
        }), undefined);
        assert.equal(JSON.stringify([1, undefined, function () {
        }]), '[1,null,null]');
        assert.equal(JSON.stringify({
            a: 1, b: function () {
            }
        }), '{"a":1}');

        var a = {};
        var b = {};
        a.collab = b;
        b.collab = a;
        assert.throws(() => {
            JSON.stringify(a);
        }, /Converting circular structure to JSON/);
        a.toJSON = function () {
            return {collab: 'b'};
        };
        assert.equal(JSON.stringify(a), '{"collab":"b"}');

        var o = {
            a: 1,
            b: 2
        };
        assert.equal(JSON.stringify(o, ['a']), '{"a":1}'); // 3rd arg does formatting
    });

    // this is one abstract operation (ToBoolean) that makes sense - make it exercise verification
    it('* -> boolean', function () {
        assert.strictEqual(Boolean(false), false);
        assert.strictEqual(Boolean(''), false);
        assert.strictEqual(Boolean(0), false);
        assert.strictEqual(Boolean(-0), false);
        assert.strictEqual(Boolean(null), false);
        assert.strictEqual(Boolean(undefined), false);
        assert.strictEqual(Boolean(NaN), false);

        // anything else is coerced to true
        assert.strictEqual(Boolean('foo'), true);
        assert.strictEqual(Boolean('false'), true);
        assert.strictEqual(Boolean('"'), true);
        assert.strictEqual(Boolean('0'), true);
        assert.strictEqual(Boolean({}), true);
        assert.strictEqual(Boolean([]), true);
        assert.strictEqual(Boolean(function () {
        }), true);
    });

    it('Seven falsy values', function () {
        assert.ok(!false);
        assert.ok(!0);
        assert.ok(!-0);
        assert.ok(!'');
        assert.ok(!NaN);
        assert.ok(!null);
        assert.ok(!undefined);

        // anything else is truthy
        assert.ok([]);
        assert.ok({});
        assert.ok(' ');
    });

    it('* -> boolean', function () {
        var foo = '123';
        var baz = Boolean(foo);

        assert.strictEqual(baz, true);

        baz = !!foo; // almost everybody does that (! - coerce and flip, ! - flip again)
        assert.strictEqual(baz, true);

        baz = foo ? true : false; // Java programmer was here
        assert.strictEqual(baz, true);
    });

    it('more * -> boolean', function () {
        var foo = '123';
        if (foo) { // ToBoolean coercion, a truthy list candidate
            assert.ok(true);
        } else {
            throw 'fail';
        }

        foo = 0;
        if (foo) {
            throw 'fail';
        } else {
            assert.ok(true);
        }

        if (foo == false) { // this is tricky, don't do that
            assert.ok(true);
        } else {
            throw 'fail';
        }

        // is foo is truthy (coercion) return foo value. if foo is falsy return second argument value
        // var baz = foo ? foo : 'foo';
        var baz = foo || 'foo'; // I wish Java had that one. Default parameters. Even implicit coercion haters do that one
        assert.equal(baz, 'foo');
    });

});