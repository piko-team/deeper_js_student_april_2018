var assert = require('assert');

/* eslint-disable no-unused-vars */
function is(a, b) {
    /* TODO: your code goes here */
}
/* eslint-enable no-unused-vars */

describe('Object.is()', function () {
    it('original implementation', function () {
        assert.ok(!Object.is(5, '5'));
        assert.ok(Object.is(NaN, NaN));
        assert.ok(Object.is('a', 'a'));
        assert.ok(!Object.is('a', 'b'));
        assert.ok(!Object.is(+0, -0));
        assert.ok(Object.is(+0, +0));
        assert.ok(Object.is(-0, -0));
    });

    it.skip('compares different value types', function () {
        assert.ok(!is(5, '5'));
    });

    it.skip('compares NaN', function () {
        assert.ok(is(NaN, NaN));
    });

    it.skip('compares same value', function () {
        assert.ok(is('a', 'a'));
    });

    it.skip('compares different values of the same type', function () {
        assert.ok(!is('a', 'b'));
    });

    it.skip('compares +0 and -0', function () {
        assert.ok(!is(+0, -0));
        assert.ok(is(+0, +0));
        assert.ok(is(-0, -0));
    });

});