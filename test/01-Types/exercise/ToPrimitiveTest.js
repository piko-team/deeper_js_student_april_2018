var assert = require('assert');

/* eslint-disable no-unused-vars */
function ToPrimitive(input, preferredType) {
    /* TODO: your code goes here */
}
/* eslint-enable no-unused-vars */

describe('ToPrimitive', function () {
    it.skip('primitive input type', function () {
        assert.strictEqual(ToPrimitive(1), 1);
        assert.strictEqual(ToPrimitive('a'), 'a');
        assert.strictEqual(ToPrimitive(null), null);
    });

    it.skip('object with valueOf happy path', function () {
        var o = {
            valueOf: function() {
                return 1;
            },
            toString: function() {
                return '1';
            }
        };

        assert.strictEqual(ToPrimitive(o, 'number'), 1);
    });

    it.skip('object with toString happy path', function () {
        var o = {
            toString: function() {
                return '1';
            },
            valueOf: function() {
                return 1;
            }
        };

        assert.strictEqual(ToPrimitive(o, 'string'), '1');
    });

    it.skip('object with valueOf unhappy path', function () {
        var o = {
            valueOf: function() {
                return {};
            },
            toString: function() {
                return '1';
            }
        };

        assert.strictEqual(ToPrimitive(o, 'number'), '1');
    });

    it.skip('object with toString unhappy path', function () {
        var o = {
            toString: function() {
                return {};
            },
            valueOf: function() {
                return 1;
            }
        };

        assert.strictEqual(ToPrimitive(o, 'string'), 1);
    });

    it.skip('input cannot be converted', function () {
        var o = {
            toString: function() {
                return {};
            },
            valueOf: function() {
                return {};
            }
        };

        assert.throws(() => {
            ToPrimitive(o);
        }, /input cannot be converted to primitive/);
    });

});