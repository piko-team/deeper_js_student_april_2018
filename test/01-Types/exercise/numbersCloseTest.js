'use strict';

var assert = require('assert');

// it's not JS specific, it's IEEE standard
describe('JS number representation is a tradeoff between accuracy and performance', function () {
    it('causing big numbers problems', function () {
        assert.ok(9999999999999999 === 10000000000000000); // use special libraries for big numbers
    });

    it('causing rounding errors', function () {
        assert.ok(0.1 + 0.2 != 0.3);
    });

    it.skip('so use number close checking helper function', function () {
        /* eslint-disable no-unused-vars */
        function numbersClose(n1, n2) {
            return /* TODO: your code goes here */;
        }
        /* eslint-enable no-unused-vars */

        assert.ok(numbersClose(0.1 + 0.2, 0.3));
        assert.ok(numbersClose(0.3, 0.1 + 0.2));
        assert.ok(!numbersClose(3, 5));
        assert.ok(!numbersClose(5, 3));
    });
});

