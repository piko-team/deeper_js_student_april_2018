var assert = require('assert');

function isReallyNaN(x) {
    return x !== x;
}

function ToBoolean(argument) {
<<<<<<< HEAD
    switch(typeof argument){
        case 'undefined': return false;
        case 'null': return false;
        case 'boolean': return (argument === true) ? true : false;
        case 'number': return (isNaN(argument) || argument == -0 || argument == +0) ? false : true;
        case 'string': return (argument == '') ? false : true;
        case 'symbol': return true;
        case 'object': return (argument == null) ? false : true;
        case 'function': return true;
    }
    return false;
=======
    if(typeof argument === 'undefined') {
        return false;
    }
    if(argument === null) {
        return false;
    }
    if(typeof argument === 'boolean') {
        return argument;
    }
    if(argument === 0 || isReallyNaN(argument)) {
        return false;
    }
    if(typeof argument === 'number') {
        return true;
    }
    if(typeof argument === 'string') {
        return argument.length === 0 ? false : true;
    }
    if(typeof argument === 'symbol') {
        return true;
    }
    if(typeof argument === 'object' || typeof argument === 'function') {
        return true;
    }
>>>>>>> 4608beb5922c92e9ac610e487dde47b578b78c81
}

describe('ToBoolean', function () {
    it('should convert undefined', function () {
        assert.strictEqual(ToBoolean(undefined), false, 'undefined conversion failed');
    });

    it('should convert null', function () {
        assert.strictEqual(ToBoolean(null), false, 'null conversion failed');
    });

    it('should convert boolean', function () {
        assert.strictEqual(ToBoolean(true), true, 'true conversion failed');
        assert.strictEqual(ToBoolean(false), false, 'false conversion failed');
    });

    it('should convert number', function () {
        assert.strictEqual(ToBoolean(+0), false, '+0 conversion failed');
        assert.strictEqual(ToBoolean(-0), false, '-0 conversion failed');
        assert.strictEqual(ToBoolean(NaN), false, 'NaN conversion failed'); // NaN is a number!!
        assert.strictEqual(ToBoolean(1), true, '1 conversion failed');
    });

    it('should convert string', function () {
        assert.strictEqual(ToBoolean(''), false, '\'\' conversion failed');
        assert.strictEqual(ToBoolean(' '), true, '\' \' conversion failed');
    });

    it('should convert symbol', function () {
        assert.strictEqual(ToBoolean(Symbol()), true, 'Symbol() conversion failed');
    });

    it('should convert object', function () {
        assert.strictEqual(ToBoolean({}), true, '{} conversion failed');
    });

    it('should convert object', function () {
        assert.strictEqual(ToBoolean(function() {}), true, 'function conversion failed');
    });
});